<?php
####################################################
# Include Theme Scripts
####################################################
add_action('wp_enqueue_scripts', function () {
	//css
	wp_enqueue_style('bootstrap-css','https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css',null,null);
	wp_enqueue_style('font-awesome-css','https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css',null,null);
	wp_enqueue_style( 'font-css', 'https://fonts.googleapis.com/css?family=Roboto' );
	wp_enqueue_style( 'style-css', get_template_directory_uri() . '/css/style.css' );
	if ( is_page_template( 'data-tmp.php' ) ) {
		wp_enqueue_style( 'table-css', get_template_directory_uri() . '/css/bootstrap-table.min.css' );
	}

	//js
	wp_enqueue_script('bootstrap-popper-js','https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js',array('jquery'),null,true);
	wp_enqueue_script('bootstrap-js','https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js',array('jquery'),null,true);
	//wp_enqueue_script( 'canvas-js', 'https://cdnjs.cloudflare.com/ajax/libs/canvasjs/1.7.0/canvasjs.min.js', array( 'jquery' ), null, true );
	wp_enqueue_script( 'main-js', get_template_directory_uri() . '/js/main.js', array( 'jquery' ), null, true );
	if ( is_page_template( 'data-tmp.php' ) ) {
		wp_enqueue_script( 'table-js', get_template_directory_uri() . '/js/bootstrap-table.min.js',array(),null,true );
		wp_enqueue_script( 'table-locale-js', get_template_directory_uri() . '/js/bootstrap-table-locale-all.min.js',array(),null,true );
		wp_enqueue_script( 'table-editable-js', 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/extensions/editable/bootstrap-table-editable.min.js',array(),null,true );
		wp_enqueue_script( 'table-export-js',  'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/extensions/export/bootstrap-table-export.min.js',array(),null,true );
		wp_enqueue_script( 'table-export2-js',  'http://rawgit.com/hhurz/tableExport.jquery.plugin/master/tableExport.js',array(),null,true );
	}
});

####################################################
# Theme Support
####################################################

add_action('after_setup_theme', function () {
	add_theme_support('title-tag');
});

####################################################
# Edit Email Header
####################################################

function website_email() {
	$sender_email= get_option('admin_email');
	return $sender_email;
}
function website_name(){
	$site_name = get_option('blogname');
	return $site_name;
}
//add_filter('wp_mail_from','website_email');
add_filter('wp_mail_from_name','website_name');

####################################################
# Upload PDF File And Get Url
####################################################

function upload_pdf_file($input_file_name){
	// These files need to be included as dependencies when on the front end.
	require_once( ABSPATH . 'wp-admin/includes/image.php' );
	require_once( ABSPATH . 'wp-admin/includes/file.php' );
	require_once( ABSPATH . 'wp-admin/includes/media.php' );

	$attachment_id = media_handle_upload( $input_file_name, 0  );
	return array(
		'url'   => wp_get_attachment_url((int)$attachment_id),
		'ID'    => $attachment_id
	);

}

####################################################
# Include PHP Files
####################################################
include 'inc/data-cpt.php';