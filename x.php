<div class="table-responsive">
	<div class="table-container">
		<table class="table">
			<thead>
			<tr>
				<th scope="col">#</th>
				<th scope="col">Full Name</th>
				<th scope="col">Company Name</th>
				<th scope="col">Email</th>
				<th scope="col">Phone</th>
				<th scope="col">checkbox checked</th>
				<th scope="col">Select Box 1</th>
				<th scope="col">Select Box 2</th>
				<th scope="col">Question1</th>
				<th scope="col">Question2</th>
				<th scope="col">Question3</th>
				<th scope="col">Question4</th>
				<th scope="col">Question5</th>
				<th scope="col">PDF File</th>
			</tr>
			</thead>
			<tbody>
			<?php
			$data =  new WP_Query(array(
				'post_type' => 'data'
			));
			while ($data->have_posts()) : $data->the_post(); ?>
				<tr>
					<th scope="row"><?php echo $data->current_post + 1 ?></th>
					<td><?php the_field('full_name'); ?><?php// print_r(get_post_meta(get_the_ID())) ?></td>
					<td><?php the_field('company_name'); ?></td>
					<td><?php the_field('email'); ?></td>
					<td><?php the_field('phone'); ?></td>
					<td>
						<p>- <?php the_field('check_box_1'); ?> And Value is: <?php the_field('check_box_1_select'); ?> %</p>
						<p>- <?php the_field('check_box_2'); ?> And Value is: <?php the_field('check_box_2_select'); ?> %</p>
						<p>- <?php the_field('check_box_3'); ?> And Value is: <?php the_field('check_box_3_select'); ?> %</p>
					</td>
					<td><?php the_field('select_box_1'); ?></td>
					<td><?php the_field('select_box_2'); ?></td>
					<td><?php the_field('question_1'); ?></td>
					<td><?php the_field('question_2'); ?></td>
					<td><?php the_field('question_3'); ?></td>
					<td><?php the_field('question_4'); ?></td>
					<td><?php the_field('question_5'); ?></td>
					<td><a href="<?php echo(get_field('pdf_file'));?>"><i class="fa fa-file"></i></a></td>
				</tr>
			<?php endwhile; wp_reset_query() ?>
			</tbody>
		</table>
	</div>
</div>