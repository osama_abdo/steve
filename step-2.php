<?php
ob_start();
//template name:Step 2
get_header();


if (isset($_POST['submit'])){

	$count = count($_POST);
	$data = $_POST;
	$text = '';
	for ($i=0;$i <= $count; $i++){
	    //$data_indexed = array_values($data)[$i];
		$text .=   array_values($data)[$i]  . "\n";
		if ($i % 2 == 1){
		    $text .= "\n";
        }
	}

	$to = get_option( 'admin_email' );
    $to = 'yasserzakaria1993@gmail.com';
	$subject = "Message From " . $data['u-email'];
    $from = sanitize_email( $_POST['u-email'] );
    $name = sanitize_text_field($_POST['u-name']);

	####################################################
	# Send Data Via Email
	####################################################
    if (empty($_FILES['pdf-file']['tmp_name']) || !is_uploaded_file($_FILES['pdf-file']['tmp_name'])) {

	    // Not There PDF File
        ####################################################
	    # Insert Data In CPT
	    ####################################################
	    $insert_data = wp_insert_post([
		    'post_type'     => 'data',
		    'post_title'    => 'Data From: ' . $name,
		    'post_content'  => $text,
		    'post_status'   => 'publish'
	    ]);

	    if (!is_wp_error($insert_data)){
		    update_field('name',sanitize_text_field($name),$insert_data);
		    update_field('email',$from,$insert_data);
		    update_field('phone',sanitize_text_field($data['u-phone']),$insert_data);
	    }
        if (wp_mail( $to, $subject, $text ) ){
	        $pid = get_page_by_path( 'step-2' )->ID;
	        wp_redirect( get_permalink( $pid ) . '/?msg=success' );
        }else{
	        $pid = get_page_by_path( 'step-2' )->ID;
	        wp_redirect( get_permalink( $pid ) . '/?msg=error' );
        }


    } elseif(!empty($_FILES['pdf-file']['tmp_name'])){
	    // There PDF File
	    if ($_FILES['pdf-file']['type'] != 'application/pdf'){
		    $pid = get_page_by_path( 'step-2' )->ID;
		    wp_redirect( get_permalink( $pid ) . '/?msg=error_file' );
		    exit();
	    }
        //المشكله عند ارسال الملف مع الفورم فقط

	    // These files need to be included as dependencies when on the front end.
	    require_once( ABSPATH . 'wp-admin/includes/image.php' );
	    require_once( ABSPATH . 'wp-admin/includes/file.php' );
	    require_once( ABSPATH . 'wp-admin/includes/media.php' );
	    $attachment_id = media_handle_upload( 'pdf-file', 0 );

        ####################################################
	    # Insert Data In CPT
	    ####################################################
	    $insert_data = wp_insert_post([
		    'post_type'     => 'data',
		    'post_title'    => 'Data From: ' . $name,
		    'post_content'  => $text,
		    'post_status'   => 'publish'
	    ]);

	    if (!is_wp_error($insert_data)){
		    update_field('name',sanitize_text_field($name),$insert_data);
		    update_field('email',$from,$insert_data);
		    update_field('phone',sanitize_text_field($data['u-phone']),$insert_data);
            $pdf_file = upload_pdf_file('pdf-file');
            update_field('pdf_file',intval($attachment_id),$insert_data);}
	    }


        if (wp_mail( $to, $subject, $text ,'',array(get_attached_file((int)$attachment_id))) ){
	        $pid = get_page_by_path( 'step-2' )->ID;
	        wp_redirect( get_permalink( $pid ) . '/?msg=success' );
        }else{
	        $pid = get_page_by_path( 'step-2' )->ID;
	        wp_redirect( get_permalink( $pid ) . '/?msg=error' );
        }

}

?>

<div id="step-2">
	<div class="container p-0">
        <?php if (isset($_GET['msg'])) : ?>
            <?php if ($_GET['msg'] == 'error_file') : ?>
                <div class="alert alert-danger text-center">
                    Sorry, PDF File Only Allow .
                </div>
            <?php endif; ?>
	        <?php if ($_GET['msg'] == 'error') : ?>
                <div class="alert alert-danger text-center">
                    Sorry, Email Not Sending Successfully, PLease Try Again Later .
                </div>
	        <?php endif; ?>
	        <?php if ($_GET['msg'] == 'success') : ?>
                <div class="alert alert-success text-center success-send">
                     Thanks, We Will Replay Shortly .
                </div>
	        <?php endif; ?>
        <?php endif; ?>
        <form action="<?php echo get_page_link(get_queried_object_id()) ; ?>" method="post" class="form-questions" enctype="multipart/form-data">
            <div id="questions-form">
                <div class="logo text-center">
                    <img src="<?php echo get_template_directory_uri() . '/img/logo.jpg'; ?>" alt="" >
                </div>

                <div class="alert alert-danger alert-dismissible alert-danger-top fade show">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    * Pleas Fill All Required Fields
                </div>
                <h3 class="title text-center text-mute">Please enter information</h3>
					<?php
					if (have_rows('step_2_qustions')) :
					while (have_rows('step_2_qustions')) : the_row(); ?>
						<div class="form-group">
                            <div class="row">
                                <div class="col-md-9">
                                    <label  for="<?php echo str_replace(' ','-' , strtolower(trim(get_sub_field('question_label')))); ?>">
                                        <?php the_sub_field('question_label') ?>

	                                    <?php if (get_sub_field('question_status') == 'required') : ?>
                                            <small class="form-text text-muted">(Required)</small>
	                                    <?php endif; ?>
	                                    <?php if (get_sub_field('question_status') != 'required') : ?>
                                            <small class="form-text text-muted">(Optional)</small>
	                                    <?php endif; ?>

                                    </label>
                                </div>
                                <div class="col-md-3">
                                    <input type="hidden" name="iput-<?php the_sub_field('question_label') ?>" value="<?php the_sub_field('question_label') ?>">
                                    <input name="<?php echo str_replace(' ','-' , strtolower(trim(get_sub_field('question_label')))); ?>" type="<?php the_sub_field('question_input_type') ?>" data-required="<?php if (get_sub_field('question_status') == 'required') {echo 'true';}else{echo 'false';} ?>" class="form-control" id="<?php echo str_replace(' ','-' , strtolower(trim(get_sub_field('question_label')))); ?>" >
                                </div>
                            </div>
						</div>
					<?php endwhile; endif; ?>


					<div class="form-group">
                        <div class="row">
                            <div class="col-md-5">
                                <label for="exampleInputFile">Upload Your PDF</label>
                            </div>
                            <div class="col-md-7">
                                <input type="file" class="form-control-file" id="pdf-file" name="pdf-file" accept="application/pdf">
                                <small id="pdf-file" class="form-text text-muted">Allow PDF Only</small>
                            </div>
                            </div>

					</div>

			</div>

			<div id="questions-contact">
					<h3 class="title text-center">
						Contact With Steve pla pla pla
					</h3>
					<div class="form-group">
                        <input type="hidden" name="person-name" value="Person Name: ">
                        <input data-required="true" type="text" class="form-control" name="u-name" placeholder="Your Name" id="name" >
                    </div>

					<div class="form-group">
                        <input type="hidden" name="person-phone" value="Person Phone: ">
                        <input data-required="true" type="text" placeholder="Your Phone" class="form-control" name="u-phone" id="phone" >
                    </div>

					<div class="form-group">
                        <input type="hidden" name="person-email" value="Person Email: ">
                        <input data-required="true" type="email" placeholder="Your Email Address" class="form-control" name="u-email" id="email" >
                    </div>

                    <div class="progress">
                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" style="width: 100%"></div>
                    </div>

                    <div class="submit text-center">
                        <button name="submit" type="submit" class="btn btn-info"> Send <i class="fa fa-send"></i> </button>
                    </div>
			</div>
		</form>

	</div>
</div>

<?php get_footer(); ob_end_flush();  ?>
