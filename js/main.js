(function ($) {

    //tooltip init
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip({
           // animation: true
        });
    });

    //tooltip chart
    function chart() {
        var chart = new CanvasJS.Chart("chartContainer",
            {
                width: 700,
                height: 400,
                title:{
                    text: "A Multi-series Column Chart"

                },

                axisX:{
                    stripLines: [
                        {
                            startValue: 15,
                            endValue: 25,
                            opacity: .3
                        }
                    ]
                },
                data: [{
                    type: "column",
                    dataPoints: [
                        { x: 10, y: 171, label:'1-2.5m $' },
                        { x: 20, y: 155, label:'2.5-5m $'},
                        { x: 30, y: 150, label:'5-10.5m $' },
                        { x: 40, y: 165, label:'10-20m $' },
                        { x: 50, y: 195 , label:'50-100m $'},
                        { x: 60, y: 168 , label:'100-250m $' },
                        { x: 70, y: 128, label:'+250m $' },
                    ]
                },
                {
                    type: "column",
                    dataPoints: [
                        { x: 10, y: 71 },
                        { x: 20, y: 55},
                        { x: 30, y: 50 },
                        { x: 40, y: 65 },
                        { x: 50, y: 95 },
                        { x: 60, y: 68 },
                        { x: 70, y: 28 },

                    ]
                }
                ]
            });

        chart.render();
    }

    //chart();

    // check if this email
    function isValidEmailAddress(emailAddress) {
        var pattern = new RegExp(/^(("[\w-+\s]+")|([\w-+]+(?:\.[\w-+]+)*)|("[\w-+\s]+")([\w-+]+(?:\.[\w-+]+)*))(@((?:[\w-+]+\.)*\w[\w-+]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][\d]\.|1[\d]{2}\.|[\d]{1,2}\.))((25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\.){2}(25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\]?$)/i);
        return pattern.test(emailAddress);
    };

    //Select Box SET Value From Text
    $('#steps .step-1-content select option').each(function () {
       $(this).val($(this).text())
    });

    //Step 1
    $('.step-1-content #next-btn').on('click',function () {
        var errors = 0;
        $('.step-1-content .alert-danger-required').remove();
        $('.step-1-content [data-required="true"]').each(function (i) {
           if (!$.trim($(this).val())){
               $(this).after("<div class='alert alert-danger alert-danger-required'>* This Field Is Required </div>");
               errors += 1 ;
           }
        });
        if (!errors > 0){
            $('#steps .steps-tabs .step-tab.active').next().addClass('active').siblings().removeClass('active');
            $('#steps .steps-content .step-content.active').hide().next().fadeIn(500);
            $('#steps .steps-content .step-content.active').removeClass('active').next().addClass('active')
        }
    });

    //Step 2
    $('.step-2-content #next-btn').on('click',function () {
            $('#steps .steps-tabs .step-tab.active').next().addClass('active').siblings().removeClass('active');
            $('#steps .steps-content .step-content.active').hide().next().fadeIn(500);
            $('#steps .steps-content .step-content.active').removeClass('active').next().addClass('active')
    });

    //Step 3
    $('.step-3-content .form-check-input').on('change',function () {
       if ($('.step-3-content .form-check-input:checked').length == 3 ){
           $('.step-3-content .form-check-input:not(:checked)').attr('disabled',true);
           $('.step-3-content .form-check-input:not(:checked)').parents('.inputs-row').find('select').attr('disabled',true);
       }else {
           $('.step-3-content .form-check-input:not(:checked)').attr('disabled',false);
           //$('.step-3-content .form-check-input:not(:checked)').parents('.inputs-row').find('select').attr('disabled',false);
       }

       if ($(this).is(':checked') ){
           var checkBoxLength = $('.step-3-content .form-check-input:checked').length;
           $(this).parents('.inputs-row').find('select').attr('name','checkbox-select-' + checkBoxLength);
           $(this).attr('name','checkbox-' + checkBoxLength);
           $(this).parents('.inputs-row').find('select').attr('disabled',false);
           $(this).parents('.inputs-row').addClass('checked');

           // add checkbox
           $(this).parents('.inputs-row').find('span.check').html('<i class="fa fa-check "></i>')

       } else{
           $(this).attr('name','');
           $(this).parents('.inputs-row').find('select').attr('name','');
           $(this).parents('.inputs-row').removeClass('checked');
           $(this).parents('.inputs-row').find('select').attr('disabled',true);

           // remove checkbox
           $(this).parents('.inputs-row').find('span.check').html('<i class="fa fa-check unchecked"></i>')
       }

    });

    $('.step-3-content #next-btn').on('click',function () {

        var percent = 0;
        $('.step-3-content .checked select').each(function (i) {
            percent += +$(this).val();
        });

        if (percent >  100 || percent < 1){
            $('.step-3-content .alert-danger').slideDown(500);
        } else{
            $('.step-3-content .alert-danger').slideUp(0);
            $('#steps .steps-tabs .step-tab.active').next().addClass('active').siblings().removeClass('active');
            $('#steps .steps-content .step-content.active').hide().next().fadeIn(500);
            $('#steps .steps-content .step-content.active').removeClass('active').next().addClass('active')
        }
    });

    //Step 4
    $('.step-4-content #next-btn').on('click',function () {
        var errors = 0;
        $('.step-4-content .alert-danger-required').remove();
        $('.step-4-content input[type="text"][data-required="true"]').each(function (i) {
            if (!$.trim($(this).val())){
                $(this).after("<div class='alert alert-danger alert-danger-required'>* This Field Is Required </div>");
                errors += 1 ;
            }
        });
        $('.step-4-content input[type="email"][data-required="true"]').each(function (i) {
            if (!$.trim($(this).val())){
                $(this).after("<div class='alert alert-danger alert-danger-required'>* This Field Is Required </div>");
                errors += 1 ;
            }else if (!isValidEmailAddress($(this).val())){
                $(this).after("<div class='alert alert-danger alert-danger-required'>* Please Enter Valid Email </div>");
                errors += 1 ;
            }
        });
        if (!errors > 0){
            $('#steps .steps-tabs .step-tab.active').next().addClass('active').siblings().removeClass('active');
            $('#steps .steps-content .step-content.active').hide().next().fadeIn(500);
            $('#steps .steps-content .step-content.active').removeClass('active').next().addClass('active')
        }
    });

    //Step 5
    $('.step-5-content #next-btn').on('click',function () {
        var errors = 0;

        $('.step-5-content .alert-danger-required').remove();
        $('.step-5-content input[type="number"][data-required="true"]').each(function (i) {
            if (!$.trim($(this).val())){
                $(this).after("<div class='alert alert-danger alert-danger-required'>* This Field Is Required </div>");
                errors += 1 ;
            }
        });

        if (!errors > 0){
            $('#steps .steps-tabs .step-tab.active').next().addClass('active').siblings().removeClass('active');
            $('#steps .steps-content .step-content.active').hide().next().fadeIn(500);
            $('#steps .steps-content .step-content.active').removeClass('active').next().addClass('active');
        }

    });

    //Step 6
    $('.step-6-content #next-btn').on('click',function () {
        var errors = 0;
        $('.step-6-content .alert-danger-required').remove();
        $('.step-6-content input[type="text"][data-required="true"]').each(function (i) {
            if (!$.trim($(this).val())){
                $(this).after("<div class='alert alert-danger alert-danger-required'>* This Field Is Required </div>");
                errors += 1 ;
            }
        });
        $('.step-6-content input[type="email"][data-required="true"]').each(function (i) {
            if (!$.trim($(this).val())){
                $(this).after("<div class='alert alert-danger alert-danger-required'>* This Field Is Required </div>");
                errors += 1 ;
            }else if (!isValidEmailAddress($(this).val())){
                $(this).after("<div class='alert alert-danger alert-danger-required'>* Please Enter Valid Email </div>");
                errors += 1 ;
            }
        });
        if (!errors > 0){
            $('#steps .step-6-content .progress-wrap').fadeIn(0)
            $('form#steps-form').submit();
        }
    });

    // check if selected fil is pdf

    $('input[type="file"].pdf-file').on('change',function () {
        $(this).next().remove('.alert');
        var fileType = '';
        if (this.files.length > 0) {
            fileType = this.files[0].type ;
        }
        if (fileType != "application/pdf"){
            $(this).after("<div class='alert alert-danger alert-danger-required'><i class='fa fa-close'></i> PDF only Allow </div>");
            $('#steps .steps-content #next-btn').attr('disabled',true);
            $('.step-5-content input[type="number"]').each(function () {
                $(this).attr('data-required','true');
            });
        }else {
            $('#steps .steps-content #next-btn').attr('disabled',false);
            $('.step-5-content input[type="number"]').each(function () {
               $(this).attr('data-required','false');
                $('.step-5-content .alert-danger-required').remove();
            });
        }
        
        if (this.files.length == 0) {
            $('#steps .steps-content #next-btn').attr('disabled',false);
            $('.step-5-content input[type="number"][data-required="true"]').attr('data-required','true');
            $('.step-5-content input[type="number"]').each(function () {
                $(this).attr('data-required','true');
            });
        }
    });

    //checkbox
    $('#steps .step-3-content .row .inputs-row span.check').on('click', function () {
        $(this).siblings('label').click();
    });

    // chart
    $('.chart-select').on('change',function () {
       var select = $(this).find(':selected').attr('data-opacity');
       $('#mning').find(select).css("opacity", "1").siblings().css("opacity", ".3");
    });

    $('.chart-select').on('change', function () {
        var column = $('.chart-select-1').find(':selected').attr('data-opacity');
        var chart  = $('.chart-select-2').find(':selected').attr('data-chart');
        $('.step-2-content').find(chart).fadeIn(0).siblings().fadeOut(0);
        $('.step-2-content').find(chart).find(column).css("opacity", "1").siblings().css("opacity", ".3");

    });

})(jQuery);