<?php
ob_start();
// template name:Home
get_header();


####################################################
# Check IF File Is PDF
####################################################

if (@$_FILES['pdf-file']['name'] != "" && @$_FILES['pdf-file']['size'] != 0 ){
    // file input not empty
    $type = $_FILES['pdf-file']['type'];
    if ($type != 'application/pdf'){
        wp_redirect(home_url('/?msg=error_file'));
        exit;
    }
}


if ( isset( $_POST['select-1'] ) ) {

    $select_1 = sanitize_text_field($_POST['select-1']);
    $select_2 = sanitize_text_field($_POST['select-2']);
    $checkbox_1 = sanitize_text_field(@$_POST['checkbox-1']);
    $checkbox_2 = sanitize_text_field(@$_POST['checkbox-2']);
    $checkbox_3 = sanitize_text_field(@$_POST['checkbox-3']);
    $checkbox_select_1 = sanitize_text_field(@$_POST['checkbox-select-1']);
    $checkbox_select_2 = sanitize_text_field(@$_POST['checkbox-select-2']);
    $checkbox_select_3 = sanitize_text_field(@$_POST['checkbox-select-3']);
    $name = sanitize_text_field($_POST['u-name']);
    $email = sanitize_email($_POST['u-email']);
    $q1 = sanitize_text_field($_POST['q-1']);
    $q2 = sanitize_text_field($_POST['q-2']);
    $q3 = sanitize_text_field($_POST['q-3']);
    $q4 = sanitize_text_field($_POST['q-4']);
    $q5 = sanitize_text_field($_POST['q-5']);
    $u_full_name = sanitize_text_field($_POST['u-full-name']);
    $u_company = sanitize_text_field($_POST['u-company']);
    $u_phone = sanitize_text_field($_POST['u-phone']);


	####################################################
	# Insert Data To Custom Post
	####################################################

    $data = wp_insert_post(array(
        'post_type'     => 'data',
        'post_title'    => 'Data From: ' . $u_full_name ,
        'post_status'   => 'publish',
    ));
	if ( ! is_wp_error( $data ) ) {
        update_field('select_box_1',$select_1,$data);
        update_field('select_box_2',$select_2,$data);
        update_field('check_box_1',$checkbox_1,$data);
        update_field('check_box_2',$checkbox_2,$data);
        update_field('check_box_3',$checkbox_2,$data);
        update_field('check_box_1_select',$checkbox_select_1,$data);
        update_field('check_box_2_select',$checkbox_select_2,$data);
        update_field('check_box_3_select',$checkbox_select_3,$data);
        update_field('name',$name,$data);
        update_field('email',$email,$data);
        update_field('question_1',$q1,$data);
        update_field('question_2',$q2,$data);
        update_field('question_3',$q3,$data);
        update_field('question_4',$q4,$data);
        update_field('question_5',$q5,$data);
        update_field('full_name',$u_full_name,$data);
        update_field('company_name',$u_company,$data);
        update_field('phone',$u_phone,$data);

        if (@$_FILES['pdf-file']['name'] != "" && @$_FILES['pdf-file']['size'] != 0 ){
            // there pdf file
	        require_once( ABSPATH . 'wp-admin/includes/image.php' );
	        require_once( ABSPATH . 'wp-admin/includes/file.php' );
	        require_once( ABSPATH . 'wp-admin/includes/media.php' );
	        $attachment_id = media_handle_upload( 'pdf-file', 0 );
	        update_field( 'pdf_file', intval( $attachment_id ), $data );
        }
		####################################################
		# Send Data To Email
		####################################################
		//$email_to   = get_option( 'admin_email' );
		$email_to   = 'anamasry101@gmail.com';
        $subject    = 'New Data From: ' . $u_full_name;
        $msg        = "Full Name: " . $u_full_name;
        $msg       .= "\n";
        $msg       .= "Company Name: " . $u_company;
		$msg       .= "\n";
        $msg       .= "Email: " . $email;
		$msg       .= "\n";
		$msg       .= "Phone: " . $u_phone;
		$msg       .= "\n";
		$msg       .= "checkbox checked: ";
		$msg       .= "\n";
		$msg       .= "1- " . $checkbox_1;
		$msg       .= "\n";
		$msg       .=  $checkbox_select_1;
		$msg       .= "\n";
		$msg       .= "2- " . $checkbox_2;
		$msg       .= "\n";
		$msg       .=  $checkbox_select_2;
		$msg       .= "\n";
		$msg       .= "3- " . $checkbox_2;
		$msg       .= "\n";
		$msg       .=  $checkbox_select_3;
		$msg       .= "\n";
		$msg       .= "Question 1";
		$msg       .= "\n";
		$msg       .= $q1;
		$msg       .= "\n";
		$msg       .= "Question 2";
		$msg       .= "\n";
		$msg       .= $q2;
		$msg       .= "\n";
		$msg       .= "Question 3";
		$msg       .= "\n";
		$msg       .= $q3;
		$msg       .= "\n";
		$msg       .= "Question 4";
		$msg       .= "\n";
		$msg       .= $q4;
		$msg       .= "\n";
		$msg       .= "Question 5";
		$msg       .= "\n";
		$msg       .= $q5;
		wp_mail($email_to,$subject,$msg,[], get_attached_file($attachment_id));
		wp_redirect(home_url('/?msg=success'));
	}
}

?>
<div class="msg">
    <div class="container">
	    <?php if (isset($_GET['msg'])) : ?>
		    <?php if ($_GET['msg'] == 'error_file') : ?>
                <div class="alert alert-danger text-center">
                    Sorry, PDF File Only Allow .
                </div>
		    <?php endif; ?>
		    <?php if ($_GET['msg'] == 'error') : ?>
                <div class="alert alert-danger text-center">
                    Sorry, Email Not Sending Successfully, PLease Try Again Later .
                </div>
		    <?php endif; ?>
		    <?php //if ($_GET['msg'] == 'success') : ?>

		    <?php //endif; ?>
	    <?php endif; ?>
    </div>
</div>

<div id="steps">
    <div class="container p-0">


        <form action="<?php echo get_page_link(get_queried_object_id()) ; ?>" method="post" id="steps-form" enctype="multipart/form-data">
            <div id="steps-container">
                <div class="logo text-center">
                    <img src="<?php echo get_template_directory_uri() . '/img/logo.jpg'; ?>" alt="" >
                </div>

                <div id="steps-form-container">
                    <div class="steps-tabs">

                        <div class="step-tab step-1-tab active" data-toggle="tooltip" title="Step 1"  data-target="step-1-content">
                            <i class="fa fa-sort-numeric-asc" aria-hidden="true"></i>
                        </div>

                        <div class="step-tab step-2-tab" data-toggle="tooltip" title="Step 2" data-target="step-2-content">
                            <i class="fa fa-bar-chart" aria-hidden="true"></i>
                        </div>

                        <div class="step-tab step-3-tab" data-toggle="tooltip" title="Step 3" data-target="step-3-content">
                            <i class="fa fa-list-ol" aria-hidden="true"></i>
                        </div>

                        <div class="step-tab step-4-tab" data-toggle="tooltip" title="Step 4" data-target="step-4-content">
                            <i class="fa fa-info" aria-hidden="true"></i>
                        </div>

                        <div class="step-tab step-5-tab" data-toggle="tooltip" title="Step 5" data-target="step-5-content">
                            <i class="fa fa-tasks" aria-hidden="true"></i>
                        </div>

                        <div class="step-tab step-6-tab" data-toggle="tooltip" title="Step 6" data-target="step-6-content">
                            <i class="fa fa-user" aria-hidden="true"></i>
                        </div>

                        <div class="step-tab step-7-tab last-step" data-toggle="tooltip" title="Step 7" data-target="step-7-content">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </div>
                        <div class="line"></div>
                    </div>

                    <div class="steps-content">

                        <div class="step-1-content step-content text-center active">
                            <div class="title">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit ?
                            </div>
                            <div class="text">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci alias aliquid, deserunt distinctio eaque exercitationem iure laudantium molestiae necessitatibus non omnis quaerat qui quisquam reiciendis repellat repellendus sunt veniam veritatis.</p>
                            </div>
                            <div class="content">
                                <div class="form-group">
                                    <div class="select">
                                        <select class="form-control chart-select chart-select-1" data-required="true" name="select-1">
                                            <option data-opacity="#q1" selected value="">$ 1-2.5m</option>
                                            <option data-opacity="#q2" value="">$ 2.5-5m</option>
                                            <option data-opacity="#q3" value="">$ 5-10m</option>
                                            <option data-opacity="#q4" value="">$ 10-20m</option>
                                            <option data-opacity="#q5" value="">$ 50-100m</option>
                                            <option data-opacity="#q6" value="">$ 100-250m</option>
                                            <option data-opacity="#q7" value="">$ +250m</option>
                                        </select>
                                    </div>

                                    <div class="select">
                                        <select class="form-control chart-select chart-select-2" data-required="true" name="select-2">
                                            <option data-chart="#agriculture" selected value="">Agriculture, Forestry, Fishing, and Hunting</option>
                                            <option data-chart="#mning" value="">Mining</option>
                                            <option data-chart="#utilities" value="">Utilities</option>
                                            <option data-chart="#construction" value="">Construction</option>
                                            <option data-chart="#manufacturing" value="">Manufacturing</option>
                                            <option data-chart="#wholesale" value="">Wholesale and Retail Trade</option>
                                            <option data-chart="#transportation" value="">Transportation and Warehousing</option>
                                            <option data-chart="#information" value="">Information</option>
                                            <option data-chart="#finance" value="">Finance and Insurance</option>
                                            <option data-chart="#real-estate" value="">Real Estate and Rental and Leasing</option>
                                            <option data-chart="#professional" value="">Professional, Scientific, and Technical Services</option>
                                            <option data-chart="#management" value="">Management of Companies (Holding Companies)</option>
                                            <option data-chart="#administrative" value="">Administrative and Support and Waste Management and Remediation Sen</option>
                                            <option data-chart="#educational" value="">Educational Services</option>
                                            <option data-chart="#arts" value="">Arts, Entertainment, and Recreation</option>
                                            <option data-chart="#healthcare" value="">Health Care and Social Assistance</option>
                                            <option data-chart="#accommodation" value="">Accommodation and Food Services</option>
                                            <option data-chart="#other" value="">Other Services</option>
                                        </select>
                                    </div>


                                </div>
                            </div>
                            <div class="next">
                                <button type="button"  id="next-btn">Next <i class="fa fa-send" aria-hidden="true"></i></button>
                            </div>
                        </div>
                        <div class="step-2-content step-content text-center">

                            <div class="charts">
                                <div style="display: block" id="agriculture">
                                    <div id="chartContainer">
                                        <table id="q-graph">

                                            <tbody>

                                            <!-- columns 1 -->
                                            <tr style="opacity: 1;" class="qtr" id="q1">
                                                <th scope="row">1 - 2.5m $</th>
                                                <td class="sent bar" style="height: 98px;"><p></p></td>
                                                <td class="paid bar" style="height: 282px;"><p></p></td>
                                            </tr>
                                            <!-- columns 2 -->
                                            <tr style="opacity: .3;" class="qtr" id="q2">
                                                <th scope="row">2.5 - 5m $</th>
                                                <td class="sent bar" style="height: 140px;"><p></p></td>
                                                <td class="paid bar" style="height: 282px;"><p></p></td>
                                            </tr>
                                            <!-- columns 3 -->
                                            <tr style="opacity: .3;" class="qtr" id="q3">
                                                <th scope="row">5 - 10m $</th>
                                                <td title="Tittle" class="sent bar" style="height: 36px"><p></p></td>
                                                <td title="Tittle"  class="paid bar" style="height: 174px"><p></p></td>
                                            </tr>
                                            <!-- columns 4 -->
                                            <tr style="opacity: .3;" class="qtr" id="q4">
                                                <th scope="row">10 - 20m $</th>
                                                <td class="sent bar" style="height: 40px;"><p></p></td>
                                                <td class="paid bar" style="height: 147px;"><p></p></td>
                                            </tr>
                                            <!-- columns 5 -->
                                            <tr style="opacity: .3;" class="qtr" id="q5">
                                                <th scope="row">50 - 100m $</th>
                                                <td title="Tittle" style="height: 15px;" class="sent bar"><p></p></td>
                                                <td title="Tittle" style="height: 65px;" class="paid bar"><p></p></td>
                                            </tr>
                                            <!-- columns 6 -->
                                            <tr style="opacity: .3;" class="qtr" id="q6">
                                                <th scope="row">100 - 250m $</th>
                                                <td class="sent bar" style="height: 35px;"><p></p></td>
                                                <td class="paid bar" style="height: 98px;"><p></p></td>
                                            </tr>
                                            <!-- columns 7 -->
                                            <tr style="opacity: .3;" class="qtr" id="q7">
                                                <th scope="row">+250m $</th>
                                                <td class="sent bar" style="height: 75px;"><p></p></td>
                                                <td class="paid bar" style="height: 155px;"><p></p></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <div id="ticks">
                                            <div class="tick" style="height: 32px;"><p>%18.0</p></div>
                                            <div class="tick" style="height: 32px;"><p>%16.0</p></div>
                                            <div class="tick" style="height: 32px;"><p>%14.0</p></div>
                                            <div class="tick" style="height: 32px;"><p>%12.0</p></div>
                                            <div class="tick" style="height: 32px;"><p>%10.0</p></div>
                                            <div class="tick" style="height: 32px;"><p>%8.0</p></div>
                                            <div class="tick" style="height: 32px;"><p>%6.0</p></div>
                                            <div class="tick" style="height: 32px;"><p>%4.0</p></div>
                                            <div class="tick" style="height: 32px;"><p>%2.0</p></div>
                                            <div class="last tick"><p>%0.0</p></div>
                                        </div>
                                    </div>
                                </div>
                                <div style="display: none" id="mning">
                                    <div id="chartContainer">
                                        <table id="q-graph">

                                            <tbody>

                                            <!-- columns 1 -->
                                            <tr style="opacity: 1;" class="qtr" id="q1">
                                                <th scope="row">1 - 2.5m $</th>
                                                <td class="sent bar" style="height: 100px;"><p></p></td>
                                                <td class="paid bar" style="height: 190px;"><p></p></td>
                                            </tr>
                                            <!-- columns 2 -->
                                            <tr style="opacity: .3;" class="qtr" id="q2">
                                                <th scope="row">2.5 - 5m $</th>
                                                <td class="sent bar" style="height: 97px;"><p></p></td>
                                                <td class="paid bar" style="height: 197px;"><p></p></td>
                                            </tr>
                                            <!-- columns 3 -->
                                            <tr style="opacity: .3;" class="qtr" id="q3">
                                                <th scope="row">5 - 10m $</th>
                                                <td title="Tittle" class="sent bar" style="height: 95px"><p></p></td>
                                                <td title="Tittle"  class="paid bar" style="height: 195px"><p></p></td>
                                            </tr>
                                            <!-- columns 4 -->
                                            <tr style="opacity: .3;" class="qtr" id="q4">
                                                <th scope="row">10 - 20m $</th>
                                                <td class="sent bar" style="height: 44px;"><p></p></td>
                                                <td class="paid bar" style="height: 220px;"><p></p></td>
                                            </tr>
                                            <!-- columns 5 -->
                                            <tr style="opacity: .3;" class="qtr" id="q5">
                                                <th scope="row">50 - 100m $</th>
                                                <td title="Tittle" style="height: 15px;" class="sent bar"><p></p></td>
                                                <td title="Tittle" style="height: 230px;" class="paid bar"><p></p></td>
                                            </tr>
                                            <!-- columns 6 -->
                                            <tr style="opacity: .3;" class="qtr" id="q6">
                                                <th scope="row">100 - 250m $</th>
                                                <td class="sent bar" style="height: 50px;"><p></p></td>
                                                <td class="paid bar" style="height: 245px;"><p></p></td>
                                            </tr>
                                            <!-- columns 7 -->
                                            <tr style="opacity: .3;" class="qtr" id="q7">
                                                <th scope="row">+250m $</th>
                                                <td class="sent bar" style="height: 44px;"><p></p></td>
                                                <td class="paid bar" style="height: 230px;"><p></p></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <div id="ticks">
                                            <!--                                        <div class="tick" style="height: 35px;"><p>%18.0</p></div>-->
                                            <!--                                        <div class="tick" style="height: 35px;"><p>%16.0</p></div>-->
                                            <!--                                        <div class="tick" style="height: 35px;"><p>%14.0</p></div>-->
                                            <div class="tick" style="height: 50px;"><p>%25.0</p></div>
                                            <div class="tick" style="height: 50px;"><p>%20.0</p></div>
                                            <div class="tick" style="height: 50px;"><p>%15.0</p></div>
                                            <div class="tick" style="height: 50px;"><p>%10.0</p></div>
                                            <div class="tick" style="height: 50px;"><p>%5.0</p></div>
                                            <div class="last tick"><p>%0.0</p></div>
                                        </div>
                                    </div>
                                </div>
                                <div style="display: none" id="utilities">
                                    <div id="chartContainer">
                                        <table id="q-graph">

                                            <tbody>

                                            <!-- columns 1 -->
                                            <tr style="opacity: 1;" class="qtr" id="q1">
                                                <th scope="row">1 - 2.5m $</th>
                                                <td class="sent bar" style="height: 128px;"><p></p></td>
                                                <td class="paid bar" style="height: 240px;"><p></p></td>
                                            </tr>
                                            <!-- columns 2 -->
                                            <tr style="opacity: .3;" class="qtr" id="q2">
                                                <th scope="row">2.5 - 5m $</th>
                                                <td class="sent bar" style="height: 50px;"><p></p></td>
                                                <td class="paid bar" style="height: 186px;"><p></p></td>
                                            </tr>
                                            <!-- columns 3 -->
                                            <tr style="opacity: .3;" class="qtr" id="q3">
                                                <th scope="row">5 - 10m $</th>
                                                <td title="Tittle" class="sent bar" style="height: 30px"><p></p></td>
                                                <td title="Tittle"  class="paid bar" style="height: 140px"><p></p></td>
                                            </tr>
                                            <!-- columns 4 -->
                                            <tr style="opacity: .3;" class="qtr" id="q4">
                                                <th scope="row">10 - 20m $</th>
                                                <td class="sent bar" style="height: 57px;"><p></p></td>
                                                <td class="paid bar" style="height: 220px;"><p></p></td>
                                            </tr>
                                            <!-- columns 5 -->
                                            <tr style="opacity: .3;" class="qtr" id="q5">
                                                <th scope="row">50 - 100m $</th>
                                                <td title="Tittle" style="height: 112px;" class="sent bar"><p></p></td>
                                                <td title="Tittle" style="height: 296px;" class="paid bar"><p></p></td>
                                            </tr>
                                            <!-- columns 6 -->
                                            <tr style="opacity: .3;" class="qtr" id="q6">
                                                <th scope="row">100 - 250m $</th>
                                                <td class="sent bar" style="height: 90px;"><p></p></td>
                                                <td class="paid bar" style="height: 245px;"><p></p></td>
                                            </tr>
                                            <!-- columns 7 -->
                                            <tr style="opacity: .3;" class="qtr" id="q7">
                                                <th scope="row">+250m $</th>
                                                <td class="sent bar" style="height: 6px;"><p></p></td>
                                                <td class="paid bar" style="height: 115px;"><p></p></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <div id="ticks">
                                            <!--                                        <div class="tick" style="height: 35px;"><p>%18.0</p></div>-->
                                            <!--                                        <div class="tick" style="height: 35px;"><p>%16.0</p></div>-->
                                            <!--                                        <div class="tick" style="height: 35px;"><p>%14.0</p></div>-->
                                            <div class="tick" style="height: 50px;top:3px"><p>%60.0</p></div>
                                            <div class="tick" style="height: 50px;"><p>%50.0</p></div>
                                            <div class="tick" style="height: 50px;"><p>%40.0</p></div>
                                            <div class="tick" style="height: 50px;"><p>%30.0</p></div>
                                            <div class="tick" style="height: 50px;"><p>%20.0</p></div>
                                            <div class="tick" style="height: 50px;"><p>%10.0</p></div>
                                            <div class="last tick"><p>%0.0</p></div>
                                        </div>
                                    </div>
                                </div>
                                <div style="display: none" id="construction">
                                    <div id="chartContainer">
                                        <table id="q-graph">

                                            <tbody>

                                            <!-- columns 1 -->
                                            <tr style="opacity: 1;" class="qtr" id="q1">
                                                <th scope="row">1 - 2.5m $</th>
                                                <td class="sent bar" style="height: 128px;"><p></p></td>
                                                <td class="paid bar" style="height: 261px;"><p></p></td>
                                            </tr>
                                            <!-- columns 2 -->
                                            <tr style="opacity: .3;" class="qtr" id="q2">
                                                <th scope="row">2.5 - 5m $</th>
                                                <td class="sent bar" style="height: 82px;"><p></p></td>
                                                <td class="paid bar" style="height: 288px;"><p></p></td>
                                            </tr>
                                            <!-- columns 3 -->
                                            <tr style="opacity: .3;" class="qtr" id="q3">
                                                <th scope="row">5 - 10m $</th>
                                                <td title="Tittle" class="sent bar" style="height: 82px"><p></p></td>
                                                <td title="Tittle"  class="paid bar" style="height: 176px"><p></p></td>
                                            </tr>
                                            <!-- columns 4 -->
                                            <tr style="opacity: .3;" class="qtr" id="q4">
                                                <th scope="row">10 - 20m $</th>
                                                <td class="sent bar" style="height: 90px;"><p></p></td>
                                                <td class="paid bar" style="height: 177px;"><p></p></td>
                                            </tr>
                                            <!-- columns 5 -->
                                            <tr style="opacity: .3;" class="qtr" id="q5">
                                                <th scope="row">50 - 100m $</th>
                                                <td title="Tittle" style="height: 65px;" class="sent bar"><p></p></td>
                                                <td title="Tittle" style="height: 150px;" class="paid bar"><p></p></td>
                                            </tr>
                                            <!-- columns 6 -->
                                            <tr style="opacity: .3;" class="qtr" id="q6">
                                                <th scope="row">100 - 250m $</th>
                                                <td class="sent bar" style="height: 66px;"><p></p></td>
                                                <td class="paid bar" style="height: 153px;"><p></p></td>
                                            </tr>
                                            <!-- columns 7 -->
                                            <tr style="opacity: .3;" class="qtr" id="q7">
                                                <th scope="row">+250m $</th>
                                                <td class="sent bar" style="height: 90px;"><p></p></td>
                                                <td class="paid bar" style="height: 175px;"><p></p></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <div id="ticks">
                                            <div class="tick" style="height: 28px;"><p>%10.0</p></div>
                                            <div class="tick" style="height: 28px;"><p>%9.0</p></div>
                                            <div class="tick" style="height: 28px;"><p>%8.0</p></div>
                                            <div class="tick" style="height: 28px;"><p>%7.0</p></div>
                                            <div class="tick" style="height: 28px;"><p>%6.0</p></div>
                                            <div class="tick" style="height: 28px;"><p>%5.0</p></div>
                                            <div class="tick" style="height: 28px;"><p>%4.0</p></div>
                                            <div class="tick" style="height: 28px;"><p>%3.0</p></div>
                                            <div class="tick" style="height: 28px;"><p>%2.0</p></div>
                                            <div class="tick" style="height: 28px;"><p>%1.0</p></div>
                                            <div class="last tick"><p>%0.0</p></div>
                                        </div>
                                    </div>
                                </div>
                                <div style="display: none" id="manufacturing">
                                    <div id="chartContainer">
                                        <table id="q-graph">

                                            <tbody>

                                            <!-- columns 1 -->
                                            <tr style="opacity: 1;" class="qtr" id="q1">
                                                <th scope="row">1 - 2.5m $</th>
                                                <td class="sent bar" style="height: 42px;"><p></p></td>
                                                <td class="paid bar" style="height: 162px;"><p></p></td>
                                            </tr>
                                            <!-- columns 2 -->
                                            <tr style="opacity: .3;" class="qtr" id="q2">
                                                <th scope="row">2.5 - 5m $</th>
                                                <td class="sent bar" style="height: 62px;"><p></p></td>
                                                <td class="paid bar" style="height: 182px;"><p></p></td>
                                            </tr>
                                            <!-- columns 3 -->
                                            <tr style="opacity: .3;" class="qtr" id="q3">
                                                <th scope="row">5 - 10m $</th>
                                                <td title="Tittle" class="sent bar" style="height: 82px"><p></p></td>
                                                <td title="Tittle"  class="paid bar" style="height: 201px"><p></p></td>
                                            </tr>
                                            <!-- columns 4 -->
                                            <tr style="opacity: .3;" class="qtr" id="q4">
                                                <th scope="row">10 - 20m $</th>
                                                <td class="sent bar" style="height: 80px;"><p></p></td>
                                                <td class="paid bar" style="height: 201px;"><p></p></td>
                                            </tr>
                                            <!-- columns 5 -->
                                            <tr style="opacity: .3;" class="qtr" id="q5">
                                                <th scope="row">50 - 100m $</th>
                                                <td title="Tittle" style="height: 80px;" class="sent bar"><p></p></td>
                                                <td title="Tittle" style="height: 195px;" class="paid bar"><p></p></td>
                                            </tr>
                                            <!-- columns 6 -->
                                            <tr style="opacity: .3;" class="qtr" id="q6">
                                                <th scope="row">100 - 250m $</th>
                                                <td class="sent bar" style="height: 80px;"><p></p></td>
                                                <td class="paid bar" style="height: 210px;"><p></p></td>
                                            </tr>
                                            <!-- columns 7 -->
                                            <tr style="opacity: .3;" class="qtr" id="q7">
                                                <th scope="row">+250m $</th>
                                                <td class="sent bar" style="height: 130px;"><p></p></td>
                                                <td class="paid bar" style="height: 264px;"><p></p></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <div id="ticks">
                                            <!--                                        <div class="tick" style="height: 35px;"><p>%18.0</p></div>-->
                                            <div class="tick" style="height: 40px;"><p>%14.0</p></div>
                                            <div class="tick" style="height: 40px;"><p>%12.0</p></div>
                                            <div class="tick" style="height: 40px;"><p>%10.0</p></div>
                                            <div class="tick" style="height: 40px;"><p>%8.0</p></div>
                                            <div class="tick" style="height: 40px;"><p>%6.0</p></div>
                                            <div class="tick" style="height: 40px;"><p>%4.0</p></div>
                                            <div class="tick" style="height: 40px;"><p>%2.0</p></div>
                                            <div class="last tick"><p>%0.0</p></div>
                                        </div>
                                    </div>
                                </div>
                                <div style="display: none" id="wholesale">
                                    <div id="chartContainer">
                                        <table id="q-graph">

                                            <tbody>

                                            <!-- columns 1 -->
                                            <tr style="opacity: 1;" class="qtr" id="q1">
                                                <th scope="row">1 - 2.5m $</th>
                                                <td class="sent bar" style="height: 115px;"><p></p></td>
                                                <td class="paid bar" style="height: 240px;"><p></p></td>
                                            </tr>
                                            <!-- columns 2 -->
                                            <tr style="opacity: .3;" class="qtr" id="q2">
                                                <th scope="row">2.5 - 5m $</th>
                                                <td class="sent bar" style="height: 92px;"><p></p></td>
                                                <td class="paid bar" style="height: 200px;"><p></p></td>
                                            </tr>
                                            <!-- columns 3 -->
                                            <tr style="opacity: .3;" class="qtr" id="q3">
                                                <th scope="row">5 - 10m $</th>
                                                <td title="Tittle" class="sent bar" style="height: 90px"><p></p></td>
                                                <td title="Tittle"  class="paid bar" style="height: 190px"><p></p></td>
                                            </tr>
                                            <!-- columns 4 -->
                                            <tr style="opacity: .3;" class="qtr" id="q4">
                                                <th scope="row">10 - 20m $</th>
                                                <td class="sent bar" style="height: 95px;"><p></p></td>
                                                <td class="paid bar" style="height: 180px;"><p></p></td>
                                            </tr>
                                            <!-- columns 5 -->
                                            <tr style="opacity: .3;" class="qtr" id="q5">
                                                <th scope="row">50 - 100m $</th>
                                                <td title="Tittle" style="height: 90px;" class="sent bar"><p></p></td>
                                                <td title="Tittle" style="height: 190px;" class="paid bar"><p></p></td>
                                            </tr>
                                            <!-- columns 6 -->
                                            <tr style="opacity: .3;" class="qtr" id="q6">
                                                <th scope="row">100 - 250m $</th>
                                                <td class="sent bar" style="height: 90px;"><p></p></td>
                                                <td class="paid bar" style="height: 180px;"><p></p></td>
                                            </tr>
                                            <!-- columns 7 -->
                                            <tr style="opacity: .3;" class="qtr" id="q7">
                                                <th scope="row">+250m $</th>
                                                <td class="sent bar" style="height: 133px;"><p></p></td>
                                                <td class="paid bar" style="height: 289px;"><p></p></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <div id="ticks">
                                            <!--                                        <div class="tick" style="height: 35px;"><p>%18.0</p></div>-->
                                            <div class="tick" style="height: 40px;"><p>%7.0</p></div>
                                            <div class="tick" style="height: 40px;"><p>%6.0</p></div>
                                            <div class="tick" style="height: 40px;"><p>%5.0</p></div>
                                            <div class="tick" style="height: 40px;"><p>%4.0</p></div>
                                            <div class="tick" style="height: 40px;"><p>%3.0</p></div>
                                            <div class="tick" style="height: 40px;"><p>%2.0</p></div>
                                            <div class="tick" style="height: 40px;"><p>%1.0</p></div>
                                            <div class="last tick"><p>%0.0</p></div>
                                        </div>
                                    </div>
                                </div>
                                <div style="display: none" id="transportation">
                                    <div id="chartContainer">
                                        <table id="q-graph">

                                            <tbody>

                                            <!-- columns 1 -->
                                            <tr style="opacity: 1;" class="qtr" id="q1">
                                                <th scope="row">1 - 2.5m $</th>
                                                <td class="sent bar" style="height: 22px;"><p></p></td>
                                                <td class="paid bar" style="height: 182px;"><p></p></td>
                                            </tr>
                                            <!-- columns 2 -->
                                            <tr style="opacity: .3;" class="qtr" id="q2">
                                                <th scope="row">2.5 - 5m $</th>
                                                <td class="sent bar" style="height: 37px;"><p></p></td>
                                                <td class="paid bar" style="height: 192px;"><p></p></td>
                                            </tr>
                                            <!-- columns 3 -->
                                            <tr style="opacity: .3;" class="qtr" id="q3">
                                                <th scope="row">5 - 10m $</th>
                                                <td title="Tittle" class="sent bar" style="height: 90px"><p></p></td>
                                                <td title="Tittle"  class="paid bar" style="height: 140px"><p></p></td>
                                            </tr>
                                            <!-- columns 4 -->
                                            <tr style="opacity: .3;" class="qtr" id="q4">
                                                <th scope="row">10 - 20m $</th>
                                                <td class="sent bar" style="height: 93px;"><p></p></td>
                                                <td class="paid bar" style="height: 170px;"><p></p></td>
                                            </tr>
                                            <!-- columns 5 -->
                                            <tr style="opacity: .3;" class="qtr" id="q5">
                                                <th scope="row">50 - 100m $</th>
                                                <td title="Tittle" style="height: 95px;" class="sent bar"><p></p></td>
                                                <td title="Tittle" style="height: 195px;" class="paid bar"><p></p></td>
                                            </tr>
                                            <!-- columns 6 -->
                                            <tr style="opacity: .3;" class="qtr" id="q6">
                                                <th scope="row">100 - 250m $</th>
                                                <td class="sent bar" style="height: 42px;"><p></p></td>
                                                <td class="paid bar" style="height: 210px;"><p></p></td>
                                            </tr>
                                            <!-- columns 7 -->
                                            <tr style="opacity: .3;" class="qtr" id="q7">
                                                <th scope="row">+250m $</th>
                                                <td class="sent bar" style="height: 77px;"><p></p></td>
                                                <td class="paid bar" style="height: 263px;"><p></p></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <div id="ticks">
                                            <div class="tick" style="height: 35px;"><p>%16.0</p></div>
                                            <div class="tick" style="height: 35px;"><p>%14.0</p></div>
                                            <div class="tick" style="height: 35px;"><p>%12.0</p></div>
                                            <div class="tick" style="height: 35px;"><p>%10.0</p></div>
                                            <div class="tick" style="height: 35px;"><p>%8.0</p></div>
                                            <div class="tick" style="height: 35px;"><p>%6.0</p></div>
                                            <div class="tick" style="height: 35px;"><p>%4.0</p></div>
                                            <div class="tick" style="height: 35px;"><p>%2.0</p></div>
                                            <div class="last tick"><p>%0.0</p></div>
                                        </div>
                                    </div>
                                </div>
                                <div style="display: none" id="information">
                                    <div id="chartContainer">
                                        <table id="q-graph">

                                            <tbody>

                                            <!-- columns 1 -->
                                            <tr style="opacity: 1;" class="qtr" id="q1">
                                                <th scope="row">1 - 2.5m $</th>
                                                <td class="sent bar" style="height: 10px;"><p></p></td>
                                                <td class="paid bar" style="height: 95px;"><p></p></td>
                                            </tr>
                                            <!-- columns 2 -->
                                            <tr style="opacity: .3;" class="qtr" id="q2">
                                                <th scope="row">2.5 - 5m $</th>
                                                <td class="sent bar" style="height: 36px;"><p></p></td>
                                                <td class="paid bar" style="height: 113px;"><p></p></td>
                                            </tr>
                                            <!-- columns 3 -->
                                            <tr style="opacity: .3;" class="qtr" id="q3">
                                                <th scope="row">5 - 10m $</th>
                                                <td title="Tittle" class="sent bar" style="height: 10px"><p></p></td>
                                                <td title="Tittle"  class="paid bar" style="height: 111px"><p></p></td>
                                            </tr>
                                            <!-- columns 4 -->
                                            <tr style="opacity: .3;" class="qtr" id="q4">
                                                <th scope="row">10 - 20m $</th>
                                                <td class="sent bar" style="height: 34px;"><p></p></td>
                                                <td class="paid bar" style="height: 122px;"><p></p></td>
                                            </tr>
                                            <!-- columns 5 -->
                                            <tr style="opacity: .3;" class="qtr" id="q5">
                                                <th scope="row">50 - 100m $</th>
                                                <td title="Tittle" style="height: 5px;" class="sent bar"><p></p></td>
                                                <td title="Tittle" style="height: 159px;" class="paid bar"><p></p></td>
                                            </tr>
                                            <!-- columns 6 -->
                                            <tr style="opacity: .3;" class="qtr" id="q6">
                                                <th scope="row">100 - 250m $</th>
                                                <td class="sent bar" style="height: 7px;"><p></p></td>
                                                <td class="paid bar" style="height: 157px;"><p></p></td>
                                            </tr>
                                            <!-- columns 7 -->
                                            <tr style="opacity: .3;" class="qtr" id="q7">
                                                <th scope="row">+250m $</th>
                                                <td class="sent bar" style="height: 105px;"><p></p></td>
                                                <td class="paid bar" style="height: 297px;"><p></p></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <div id="ticks">
                                            <div class="tick" style="height: 55px;"><p>%25.0</p></div>
                                            <div class="tick" style="height: 55px;"><p>%20.0</p></div>
                                            <div class="tick" style="height: 55px;"><p>%15.0</p></div>
                                            <div class="tick" style="height: 55px;"><p>%10.0</p></div>
                                            <div class="tick" style="height: 55px;"><p>%5.0</p></div>
                                            <div class="last tick"><p>%0.0</p></div>
                                        </div>
                                    </div>
                                </div>
                                <div style="display: none" id="finance">
                                    <div id="chartContainer">
                                        <table id="q-graph">

                                            <tbody>

                                            <!-- columns 1 -->
                                            <tr style="opacity: 1;" class="qtr" id="q1">
                                                <th scope="row">1 - 2.5m $</th>
                                                <td class="sent bar" style="height: 105px;"><p></p></td>
                                                <td class="paid bar" style="height: 146px;"><p></p></td>
                                            </tr>
                                            <!-- columns 2 -->
                                            <tr style="opacity: .3;" class="qtr" id="q2">
                                                <th scope="row">2.5 - 5m $</th>
                                                <td class="sent bar" style="height: 107px;"><p></p></td>
                                                <td class="paid bar" style="height: 155px;"><p></p></td>
                                            </tr>
                                            <!-- columns 3 -->
                                            <tr style="opacity: .3;" class="qtr" id="q3">
                                                <th scope="row">5 - 10m $</th>
                                                <td title="Tittle" class="sent bar" style="height: 143px"><p></p></td>
                                                <td title="Tittle"  class="paid bar" style="height: 187px"><p></p></td>
                                            </tr>
                                            <!-- columns 4 -->
                                            <tr style="opacity: .3;" class="qtr" id="q4">
                                                <th scope="row">10 - 20m $</th>
                                                <td class="sent bar" style="height: 215px;"><p></p></td>
                                                <td class="paid bar" style="height: 250px;"><p></p></td>
                                            </tr>
                                            <!-- columns 5 -->
                                            <tr style="opacity: .3;" class="qtr" id="q5">
                                                <th scope="row">50 - 100m $</th>
                                                <td title="Tittle" style="height: 5px;" class="sent bar"><p></p></td>
                                                <td title="Tittle" style="height: 159px;" class="paid bar"><p></p></td>
                                            </tr>
                                            <!-- columns 6 -->
                                            <tr style="opacity: .3;" class="qtr" id="q6">
                                                <th scope="row">100 - 250m $</th>
                                                <td class="sent bar" style="height: 248px;"><p></p></td>
                                                <td class="paid bar" style="height: 287px;"><p></p></td>
                                            </tr>
                                            <!-- columns 7 -->
                                            <tr style="opacity: .3;" class="qtr" id="q7">
                                                <th scope="row">+250m $</th>
                                                <td class="sent bar" style="height: 72px;"><p></p></td>
                                                <td class="paid bar" style="height: 121px;"><p></p></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <div id="ticks">
                                            <div class="tick" style="height: 30px;"><p>%50.0</p></div>
                                            <div class="tick" style="height: 30px;"><p>%45.0</p></div>
                                            <div class="tick" style="height: 30px;"><p>%40.0</p></div>
                                            <div class="tick" style="height: 30px;"><p>%35.0</p></div>
                                            <div class="tick" style="height: 30px;"><p>%30.0</p></div>
                                            <div class="tick" style="height: 30px;"><p>%25.0</p></div>
                                            <div class="tick" style="height: 30px;"><p>%20.0</p></div>
                                            <div class="tick" style="height: 30px;"><p>%15.0</p></div>
                                            <div class="tick" style="height: 30px;"><p>%10.0</p></div>
                                            <div class="tick" style="height: 30px;"><p>%5.0</p></div>
                                            <div class="last tick"><p>%0.0</p></div>
                                        </div>
                                    </div>
                                </div>
                                <div style="display: none" id="real-estate">
                                    <div id="chartContainer">
                                        <table id="q-graph">

                                            <tbody>

                                            <!-- columns 1 -->
                                            <tr style="opacity: 1;" class="qtr" id="q1">
                                                <th scope="row">1 - 2.5m $</th>
                                                <td class="sent bar" style="height: 85px;"><p></p></td>
                                                <td class="paid bar" style="height: 195px;"><p></p></td>
                                            </tr>
                                            <!-- columns 2 -->
                                            <tr style="opacity: .3;" class="qtr" id="q2">
                                                <th scope="row">2.5 - 5m $</th>
                                                <td class="sent bar" style="height: 57px;"><p></p></td>
                                                <td class="paid bar" style="height: 165px;"><p></p></td>
                                            </tr>
                                            <!-- columns 3 -->
                                            <tr style="opacity: .3;" class="qtr" id="q3">
                                                <th scope="row">5 - 10m $</th>
                                                <td title="Tittle" class="sent bar" style="height: 85px"><p></p></td>
                                                <td title="Tittle"  class="paid bar" style="height: 235px"><p></p></td>
                                            </tr>
                                            <!-- columns 4 -->
                                            <tr style="opacity: .3;" class="qtr" id="q4">
                                                <th scope="row">10 - 20m $</th>
                                                <td class="sent bar" style="height: 50px;"><p></p></td>
                                                <td class="paid bar" style="height: 235px;"><p></p></td>
                                            </tr>
                                            <!-- columns 5 -->
                                            <tr style="opacity: .3;" class="qtr" id="q5">
                                                <th scope="row">50 - 100m $</th>
                                                <td title="Tittle" style="height: 44px;" class="sent bar"><p></p></td>
                                                <td title="Tittle" style="height: 245px;" class="paid bar"><p></p></td>
                                            </tr>
                                            <!-- columns 6 -->
                                            <tr style="opacity: .3;" class="qtr" id="q6">
                                                <th scope="row">100 - 250m $</th>
                                                <td class="sent bar" style="height: 40px;"><p></p></td>
                                                <td class="paid bar" style="height: 238px;"><p></p></td>
                                            </tr>
                                            <!-- columns 7 -->
                                            <tr style="opacity: .3;" class="qtr" id="q7">
                                                <th scope="row">+250m $</th>
                                                <td class="sent bar" style="height: 45px;"><p></p></td>
                                                <td class="paid bar" style="height: 121px;"><p></p></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <div id="ticks">
                                            <div class="tick" style="height: 45px;top:6px"><p>%35.0</p></div>
                                            <div class="tick" style="height: 45px;top: -8px;"><p>%30.0</p></div>
                                            <div class="tick" style="height: 45px;"><p>%25.0</p></div>
                                            <div class="tick" style="height: 45px;"><p>%20.0</p></div>
                                            <div class="tick" style="height: 45px;"><p>%15.0</p></div>
                                            <div class="tick" style="height: 45px;"><p>%10.0</p></div>
                                            <div class="tick" style="height: 45px;"><p>%5.0</p></div>
                                            <div class="last tick"><p>%0.0</p></div>
                                        </div>
                                    </div>
                                </div>
                                <div style="display: none" id="professional">
                                    <div id="chartContainer">
                                        <table id="q-graph">

                                            <tbody>

                                            <!-- columns 1 -->
                                            <tr style="opacity: 1;" class="qtr" id="q1">
                                                <th scope="row">1 - 2.5m $</th>
                                                <td class="sent bar" style="height: 145px;"><p></p></td>
                                                <td class="paid bar" style="height: 265px;"><p></p></td>
                                            </tr>
                                            <!-- columns 2 -->
                                            <tr style="opacity: .3;" class="qtr" id="q2">
                                                <th scope="row">2.5 - 5m $</th>
                                                <td class="sent bar" style="height: 104px;"><p></p></td>
                                                <td class="paid bar" style="height: 220px;"><p></p></td>
                                            </tr>
                                            <!-- columns 3 -->
                                            <tr style="opacity: .3;" class="qtr" id="q3">
                                                <th scope="row">5 - 10m $</th>
                                                <td title="Tittle" class="sent bar" style="height: 97px"><p></p></td>
                                                <td title="Tittle"  class="paid bar" style="height: 205px"><p></p></td>
                                            </tr>
                                            <!-- columns 4 -->
                                            <tr style="opacity: .3;" class="qtr" id="q4">
                                                <th scope="row">10 - 20m $</th>
                                                <td class="sent bar" style="height: 70px;"><p></p></td>
                                                <td class="paid bar" style="height: 194px;"><p></p></td>
                                            </tr>
                                            <!-- columns 5 -->
                                            <tr style="opacity: .3;" class="qtr" id="q5">
                                                <th scope="row">50 - 100m $</th>
                                                <td title="Tittle" style="height: 53px;" class="sent bar"><p></p></td>
                                                <td title="Tittle" style="height: 202px;" class="paid bar"><p></p></td>
                                            </tr>
                                            <!-- columns 6 -->
                                            <tr style="opacity: .3;" class="qtr" id="q6">
                                                <th scope="row">100 - 250m $</th>
                                                <td class="sent bar" style="height: 77px;"><p></p></td>
                                                <td class="paid bar" style="height: 245px;"><p></p></td>
                                            </tr>
                                            <!-- columns 7 -->
                                            <tr style="opacity: .3;" class="qtr" id="q7">
                                                <th scope="row">+250m $</th>
                                                <td class="sent bar" style="height: 98px;"><p></p></td>
                                                <td class="paid bar" style="height: 286px;"><p></p></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <div id="ticks">
                                            <div class="tick" style="height: 48px;"><p>%12.0</p></div>
                                            <div class="tick" style="height: 48px;"><p>%10.0</p></div>
                                            <div class="tick" style="height: 48px;"><p>%8.0</p></div>
                                            <div class="tick" style="height: 48px;"><p>%6.0</p></div>
                                            <div class="tick" style="height: 48px;"><p>%4.0</p></div>
                                            <div class="tick" style="height: 48px;"><p>%2.0</p></div>
                                            <div class="last tick"><p>%0.0</p></div>
                                        </div>
                                    </div>
                                </div>
                                <div style="display: none" id="management">
                                    <div id="chartContainer">
                                        <table id="q-graph">

                                            <tbody>

                                            <!-- columns 1 -->
                                            <tr style="opacity: 1;" class="qtr" id="q1">
                                                <th scope="row">1 - 2.5m $</th>
                                                <td class="sent bar" style="height: 177px;"><p></p></td>
                                                <td class="paid bar" style="height: 288px;"><p></p></td>
                                            </tr>
                                            <!-- columns 2 -->
                                            <tr style="opacity: .3;" class="qtr" id="q2">
                                                <th scope="row">2.5 - 5m $</th>
                                                <td class="sent bar" style="height: 123px;"><p></p></td>
                                                <td class="paid bar" style="height: 215px;"><p></p></td>
                                            </tr>
                                            <!-- columns 3 -->
                                            <tr style="opacity: .3;" class="qtr" id="q3">
                                                <th scope="row">5 - 10m $</th>
                                                <td title="Tittle" class="sent bar" style="height: 132px"><p></p></td>
                                                <td title="Tittle"  class="paid bar" style="height: 233px"><p></p></td>
                                            </tr>
                                            <!-- columns 4 -->
                                            <tr style="opacity: .3;" class="qtr" id="q4">
                                                <th scope="row">10 - 20m $</th>
                                                <td class="sent bar" style="height: 128px;"><p></p></td>
                                                <td class="paid bar" style="height: 244px;"><p></p></td>
                                            </tr>
                                            <!-- columns 5 -->
                                            <tr style="opacity: .3;" class="qtr" id="q5">
                                                <th scope="row">50 - 100m $</th>
                                                <td title="Tittle" style="height: 182px;" class="sent bar"><p></p></td>
                                                <td title="Tittle" style="height: 288px;" class="paid bar"><p></p></td>
                                            </tr>
                                            <!-- columns 6 -->
                                            <tr style="opacity: .3;" class="qtr" id="q6">
                                                <th scope="row">100 - 250m $</th>
                                                <td class="sent bar" style="height: 158px;"><p></p></td>
                                                <td class="paid bar" style="height: 270px;"><p></p></td>
                                            </tr>
                                            <!-- columns 7 -->
                                            <tr style="opacity: .3;" class="qtr" id="q7">
                                                <th scope="row">+250m $</th>
                                                <td class="sent bar" style="height: 120px;"><p></p></td>
                                                <td class="paid bar" style="height: 234px;"><p></p></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <div id="ticks">
                                            <div class="tick" style="height: 30px;"><p>%50.0</p></div>
                                            <div class="tick" style="height: 30px;"><p>%45.0</p></div>
                                            <div class="tick" style="height: 30px;"><p>%40.0</p></div>
                                            <div class="tick" style="height: 30px;"><p>%35.0</p></div>
                                            <div class="tick" style="height: 30px;"><p>%30.0</p></div>
                                            <div class="tick" style="height: 30px;"><p>%25.0</p></div>
                                            <div class="tick" style="height: 30px;"><p>%20.0</p></div>
                                            <div class="tick" style="height: 30px;"><p>%15.0</p></div>
                                            <div class="tick" style="height: 30px;"><p>%10.0</p></div>
                                            <div class="tick" style="height: 30px;"><p>%5.0</p></div>
                                            <div class="last tick"><p>%0.0</p></div>
                                        </div>
                                    </div>
                                </div>
                                <div style="display: none" id="administrative">
                                    <div id="chartContainer">
                                        <table id="q-graph">
                                            <tbody>
                                            <!-- columns 1 -->
                                            <tr style="opacity: 1;" class="qtr" id="q1">
                                                <th scope="row">1 - 2.5m $</th>
                                                <td class="sent bar" style="height: 75px;"><p></p></td>
                                                <td class="paid bar" style="height: 208px;"><p></p></td>
                                            </tr>
                                            <!-- columns 2 -->
                                            <tr style="opacity: .3;" class="qtr" id="q2">
                                                <th scope="row">2.5 - 5m $</th>
                                                <td class="sent bar" style="height: 84px;"><p></p></td>
                                                <td class="paid bar" style="height: 195px;"><p></p></td>
                                            </tr>
                                            <!-- columns 3 -->
                                            <tr style="opacity: .3;" class="qtr" id="q3">
                                                <th scope="row">5 - 10m $</th>
                                                <td title="Tittle" class="sent bar" style="height: 88px"><p></p></td>
                                                <td title="Tittle"  class="paid bar" style="height: 195px"><p></p></td>
                                            </tr>
                                            <!-- columns 4 -->
                                            <tr style="opacity: .3;" class="qtr" id="q4">
                                                <th scope="row">10 - 20m $</th>
                                                <td class="sent bar" style="height: 61px;"><p></p></td>
                                                <td class="paid bar" style="height: 190px;"><p></p></td>
                                            </tr>
                                            <!-- columns 5 -->
                                            <tr style="opacity: .3;" class="qtr" id="q5">
                                                <th scope="row">50 - 100m $</th>
                                                <td title="Tittle" style="height: 56px;" class="sent bar"><p></p></td>
                                                <td title="Tittle" style="height: 191px;" class="paid bar"><p></p></td>
                                            </tr>
                                            <!-- columns 6 -->
                                            <tr style="opacity: .3;" class="qtr" id="q6">
                                                <th scope="row">100 - 250m $</th>
                                                <td class="sent bar" style="height: 25px;"><p></p></td>
                                                <td class="paid bar" style="height: 190px;"><p></p></td>
                                            </tr>
                                            <!-- columns 7 -->
                                            <tr style="opacity: .3;" class="qtr" id="q7">
                                                <th scope="row">+250m $</th>
                                                <td class="sent bar" style="height: 78px;"><p></p></td>
                                                <td class="paid bar" style="height: 286px;"><p></p></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <div id="ticks">
                                            <div class="tick" style="height: 40px;"><p>%14.0</p></div>
                                            <div class="tick" style="height: 40px;"><p>%12.0</p></div>
                                            <div class="tick" style="height: 40px;"><p>%10.0</p></div>
                                            <div class="tick" style="height: 40px;"><p>%8.0</p></div>
                                            <div class="tick" style="height: 40px;"><p>%6.0</p></div>
                                            <div class="tick" style="height: 40px;"><p>%4.0</p></div>
                                            <div class="tick" style="height: 40px;"><p>%2.0</p></div>
                                            <div class="last tick"><p>%0.0</p></div>
                                        </div>
                                    </div>
                                </div>
                                <div style="display: none" id="educational">
                                    <div id="chartContainer">
                                        <table id="q-graph">
                                            <tbody>
                                            <!-- columns 1 -->
                                            <tr style="opacity: 1;" class="qtr" id="q1">
                                                <th scope="row">1 - 2.5m $</th>
                                                <td class="sent bar" style="height: 38px;"><p></p></td>
                                                <td class="paid bar" style="height: 168px;"><p></p></td>
                                            </tr>
                                            <!-- columns 2 -->
                                            <tr style="opacity: .3;" class="qtr" id="q2">
                                                <th scope="row">2.5 - 5m $</th>
                                                <td class="sent bar" style="height: 99px;"><p></p></td>
                                                <td class="paid bar" style="height: 170px;"><p></p></td>
                                            </tr>
                                            <!-- columns 3 -->
                                            <tr style="opacity: .3;" class="qtr" id="q3">
                                                <th scope="row">5 - 10m $</th>
                                                <td title="Tittle" class="sent bar" style="height: 90px"><p></p></td>
                                                <td title="Tittle"  class="paid bar" style="height: 162px"><p></p></td>
                                            </tr>
                                            <!-- columns 4 -->
                                            <tr style="opacity: .3;" class="qtr" id="q4">
                                                <th scope="row">10 - 20m $</th>
                                                <td class="sent bar" style="height: 66px;"><p></p></td>
                                                <td class="paid bar" style="height: 170px;"><p></p></td>
                                            </tr>
                                            <!-- columns 5 -->
                                            <tr style="opacity: .3;" class="qtr" id="q5">
                                                <th scope="row">50 - 100m $</th>
                                                <td title="Tittle" style="height: 15px;" class="sent bar"><p></p></td>
                                                <td title="Tittle" style="height: 235px;" class="paid bar"><p></p></td>
                                            </tr>
                                            <!-- columns 6 -->
                                            <tr style="opacity: .3;" class="qtr" id="q6">
                                                <th scope="row">100 - 250m $</th>
                                                <td class="sent bar" style="height: 100px;"><p></p></td>
                                                <td class="paid bar" style="height: 290px;"><p></p></td>
                                            </tr>
                                            <!-- columns 7 -->
                                            <tr style="opacity: .3;" class="qtr" id="q7">
                                                <th scope="row">+250m $</th>
                                                <td class="sent bar" style="height: 111px;"><p></p></td>
                                                <td class="paid bar" style="height: 264px;"><p></p></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <div id="ticks">
                                            <div class="tick" style="height: 35px;"><p>%16.0</p></div>
                                            <div class="tick" style="height: 35px;"><p>%14.0</p></div>
                                            <div class="tick" style="height: 35px;"><p>%12.0</p></div>
                                            <div class="tick" style="height: 35px;"><p>%10.0</p></div>
                                            <div class="tick" style="height: 35px;"><p>%8.0</p></div>
                                            <div class="tick" style="height: 35px;"><p>%6.0</p></div>
                                            <div class="tick" style="height: 35px;"><p>%4.0</p></div>
                                            <div class="tick" style="height: 35px;"><p>%2.0</p></div>
                                            <div class="last tick"><p>%0.0</p></div>
                                        </div>
                                    </div>
                                </div>
                                <div style="display: none" id="arts">
                                    <div id="chartContainer">
                                        <table id="q-graph">
                                            <tbody>
                                            <!-- columns 1 -->
                                            <tr style="opacity: 1;" class="qtr" id="q1">
                                                <th scope="row">1 - 2.5m $</th>
                                                <td class="sent bar" style="height: 43px;"><p></p></td>
                                                <td class="paid bar" style="height: 117px;"><p></p></td>
                                            </tr>
                                            <!-- columns 2 -->
                                            <tr style="opacity: .3;" class="qtr" id="q2">
                                                <th scope="row">2.5 - 5m $</th>
                                                <td class="sent bar" style="height: 53px;"><p></p></td>
                                                <td class="paid bar" style="height: 111px;"><p></p></td>
                                            </tr>
                                            <!-- columns 3 -->
                                            <tr style="opacity: .3;" class="qtr" id="q3">
                                                <th scope="row">5 - 10m $</th>
                                                <td title="Tittle" class="sent bar" style="height: 40px"><p></p></td>
                                                <td title="Tittle"  class="paid bar" style="height: 99px"><p></p></td>
                                            </tr>
                                            <!-- columns 4 -->
                                            <tr style="opacity: .3;" class="qtr" id="q4">
                                                <th scope="row">10 - 20m $</th>
                                                <td class="sent bar" style="height: 80px;"><p></p></td>
                                                <td class="paid bar" style="height: 177px;"><p></p></td>
                                            </tr>
                                            <!-- columns 5 -->
                                            <tr style="opacity: .3;" class="qtr" id="q5">
                                                <th scope="row">50 - 100m $</th>
                                                <td title="Tittle" style="height: 40px;" class="sent bar"><p></p></td>
                                                <td title="Tittle" style="height: 164px;" class="paid bar"><p></p></td>
                                            </tr>
                                            <!-- columns 6 -->
                                            <tr style="opacity: .3;" class="qtr" id="q6">
                                                <th scope="row">100 - 250m $</th>
                                                <td class="sent bar" style="height: 60px;"><p></p></td>
                                                <td class="paid bar" style="height: 220px;"><p></p></td>
                                            </tr>
                                            <!-- columns 7 -->
                                            <tr style="opacity: .3;" class="qtr" id="q7">
                                                <th scope="row">+250m $</th>
                                                <td class="sent bar" style="height: 20px;"><p></p></td>
                                                <td class="paid bar" style="height: 280px;"><p></p></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <div id="ticks">
                                            <div class="tick" style="height: 52px;"><p>%25.0</p></div>
                                            <div class="tick" style="height: 52px;"><p>%20.0</p></div>
                                            <div class="tick" style="height: 52px;"><p>%15.0</p></div>
                                            <div class="tick" style="height: 52px;"><p>%10.0</p></div>
                                            <div class="tick" style="height: 52px;"><p>%5.0</p></div>
                                            <div class="last tick"><p>%0.0</p></div>
                                        </div>
                                    </div>
                                </div>
                                <div style="display: none" id="healthcare">
                                    <div id="chartContainer">
                                        <table id="q-graph">
                                            <tbody>
                                            <!-- columns 1 -->
                                            <tr style="opacity: 1;" class="qtr" id="q1">
                                                <th scope="row">1 - 2.5m $</th>
                                                <td class="sent bar" style="height: 160px;"><p></p></td>
                                                <td class="paid bar" style="height: 265px;"><p></p></td>
                                            </tr>
                                            <!-- columns 2 -->
                                            <tr style="opacity: .3;" class="qtr" id="q2">
                                                <th scope="row">2.5 - 5m $</th>
                                                <td class="sent bar" style="height: 124px;"><p></p></td>
                                                <td class="paid bar" style="height: 222px;"><p></p></td>
                                            </tr>
                                            <!-- columns 3 -->
                                            <tr style="opacity: .3;" class="qtr" id="q3">
                                                <th scope="row">5 - 10m $</th>
                                                <td title="Tittle" class="sent bar" style="height: 92px"><p></p></td>
                                                <td title="Tittle"  class="paid bar" style="height: 170px"><p></p></td>
                                            </tr>
                                            <!-- columns 4 -->
                                            <tr style="opacity: .3;" class="qtr" id="q4">
                                                <th scope="row">10 - 20m $</th>
                                                <td class="sent bar" style="height: 35px;"><p></p></td>
                                                <td class="paid bar" style="height: 126px;"><p></p></td>
                                            </tr>
                                            <!-- columns 5 -->
                                            <tr style="opacity: .3;" class="qtr" id="q5">
                                                <th scope="row">50 - 100m $</th>
                                                <td title="Tittle" style="height: 17px;" class="sent bar"><p></p></td>
                                                <td title="Tittle" style="height: 132px;" class="paid bar"><p></p></td>
                                            </tr>
                                            <!-- columns 6 -->
                                            <tr style="opacity: .3;" class="qtr" id="q6">
                                                <th scope="row">100 - 250m $</th>
                                                <td class="sent bar" style="height: 31px;"><p></p></td>
                                                <td class="paid bar" style="height: 175px;"><p></p></td>
                                            </tr>
                                            <!-- columns 7 -->
                                            <tr style="opacity: .3;" class="qtr" id="q7">
                                                <th scope="row">+250m $</th>
                                                <td class="sent bar" style="height: 51px;"><p></p></td>
                                                <td class="paid bar" style="height: 260px;"><p></p></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <div id="ticks">
                                            <div class="tick" style="height: 35px;"><p>%16.0</p></div>
                                            <div class="tick" style="height: 35px;"><p>%14.0</p></div>
                                            <div class="tick" style="height: 35px;"><p>%12.0</p></div>
                                            <div class="tick" style="height: 35px;"><p>%10.0</p></div>
                                            <div class="tick" style="height: 35px;"><p>%8.0</p></div>
                                            <div class="tick" style="height: 35px;"><p>%6.0</p></div>
                                            <div class="tick" style="height: 35px;"><p>%4.0</p></div>
                                            <div class="tick" style="height: 35px;"><p>%2.0</p></div>
                                            <div class="last tick"><p>%0.0</p></div>
                                        </div>
                                    </div>
                                </div>
                                <div style="display: none" id="accommodation">
                                    <div id="chartContainer">
                                        <table id="q-graph">
                                            <tbody>
                                            <!-- columns 1 -->
                                            <tr style="opacity: 1;" class="qtr" id="q1">
                                                <th scope="row">1 - 2.5m $</th>
                                                <td class="sent bar" style="height: 50px;"><p></p></td>
                                                <td class="paid bar" style="height: 160px;"><p></p></td>
                                            </tr>
                                            <!-- columns 2 -->
                                            <tr style="opacity: .3;" class="qtr" id="q2">
                                                <th scope="row">2.5 - 5m $</th>
                                                <td class="sent bar" style="height: 55px;"><p></p></td>
                                                <td class="paid bar" style="height: 155px;"><p></p></td>
                                            </tr>
                                            <!-- columns 3 -->
                                            <tr style="opacity: .3;" class="qtr" id="q3">
                                                <th scope="row">5 - 10m $</th>
                                                <td title="Tittle" class="sent bar" style="height: 53px"><p></p></td>
                                                <td title="Tittle"  class="paid bar" style="height: 150px"><p></p></td>
                                            </tr>
                                            <!-- columns 4 -->
                                            <tr style="opacity: .3;" class="qtr" id="q4">
                                                <th scope="row">10 - 20m $</th>
                                                <td class="sent bar" style="height: 53px;"><p></p></td>
                                                <td class="paid bar" style="height: 160px;"><p></p></td>
                                            </tr>
                                            <!-- columns 5 -->
                                            <tr style="opacity: .3;" class="qtr" id="q5">
                                                <th scope="row">50 - 100m $</th>
                                                <td title="Tittle" style="height: 73px;" class="sent bar"><p></p></td>
                                                <td title="Tittle" style="height: 203px;" class="paid bar"><p></p></td>
                                            </tr>
                                            <!-- columns 6 -->
                                            <tr style="opacity: .3;" class="qtr" id="q6">
                                                <th scope="row">100 - 250m $</th>
                                                <td class="sent bar" style="height: 12px;"><p></p></td>
                                                <td class="paid bar" style="height: 160px;"><p></p></td>
                                            </tr>
                                            <!-- columns 7 -->
                                            <tr style="opacity: .3;" class="qtr" id="q7">
                                                <th scope="row">+250m $</th>
                                                <td class="sent bar" style="height: 105px;"><p></p></td>
                                                <td class="paid bar" style="height: 268px;"><p></p></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <div id="ticks">
                                            <div class="tick" style="height: 62px;"><p>%20.0</p></div>
                                            <div class="tick" style="height: 62px;"><p>%15.0</p></div>
                                            <div class="tick" style="height: 62px;"><p>%10.0</p></div>
                                            <div class="tick" style="height: 62px;"><p>%5.0</p></div>
                                            <div class="last tick"><p>%0.0</p></div>
                                        </div>
                                    </div>
                                </div>
                                <div style="display: none" id="other">
                                    <div id="chartContainer">
                                        <table id="q-graph">
                                            <tbody>
                                            <!-- columns 1 -->
                                            <tr style="opacity: 1;" class="qtr" id="q1">
                                                <th scope="row">1 - 2.5m $</th>
                                                <td class="sent bar" style="height: 73px;"><p></p></td>
                                                <td class="paid bar" style="height: 197px;"><p></p></td>
                                            </tr>
                                            <!-- columns 2 -->
                                            <tr style="opacity: .3;" class="qtr" id="q2">
                                                <th scope="row">2.5 - 5m $</th>
                                                <td class="sent bar" style="height: 99px;"><p></p></td>
                                                <td class="paid bar" style="height: 195px;"><p></p></td>
                                            </tr>
                                            <!-- columns 3 -->
                                            <tr style="opacity: .3;" class="qtr" id="q3">
                                                <th scope="row">5 - 10m $</th>
                                                <td title="Tittle" class="sent bar" style="height: 57px"><p></p></td>
                                                <td title="Tittle"  class="paid bar" style="height: 160px"><p></p></td>
                                            </tr>
                                            <!-- columns 4 -->
                                            <tr style="opacity: .3;" class="qtr" id="q4">
                                                <th scope="row">10 - 20m $</th>
                                                <td class="sent bar" style="height: 54px;"><p></p></td>
                                                <td class="paid bar" style="height: 180px;"><p></p></td>
                                            </tr>
                                            <!-- columns 5 -->
                                            <tr style="opacity: .3;" class="qtr" id="q5">
                                                <th scope="row">50 - 100m $</th>
                                                <td title="Tittle" style="height: 64px;" class="sent bar"><p></p></td>
                                                <td title="Tittle" style="height: 178px;" class="paid bar"><p></p></td>
                                            </tr>
                                            <!-- columns 6 -->
                                            <tr style="opacity: .3;" class="qtr" id="q6">
                                                <th scope="row">100 - 250m $</th>
                                                <td class="sent bar" style="height: 30px;"><p></p></td>
                                                <td class="paid bar" style="height: 197px;"><p></p></td>
                                            </tr>
                                            <!-- columns 7 -->
                                            <tr style="opacity: .3;" class="qtr" id="q7">
                                                <th scope="row">+250m $</th>
                                                <td class="sent bar" style="height: 72px;"><p></p></td>
                                                <td class="paid bar" style="height: 280px;"><p></p></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <div id="ticks">
                                            <div class="tick" style="height: 35px;"><p>%16.0</p></div>
                                            <div class="tick" style="height: 35px;"><p>%14.0</p></div>
                                            <div class="tick" style="height: 35px;"><p>%12.0</p></div>
                                            <div class="tick" style="height: 35px;"><p>%10.0</p></div>
                                            <div class="tick" style="height: 35px;"><p>%8.0</p></div>
                                            <div class="tick" style="height: 35px;"><p>%6.0</p></div>
                                            <div class="tick" style="height: 35px;"><p>%4.0</p></div>
                                            <div class="tick" style="height: 35px;"><p>%2.0</p></div>
                                            <div class="last tick"><p>%0.0</p></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="text">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis consequuntur ex facere laborum maiores praesentium tempora ut. Alias at ex ipsam ipsum minima, nobis optio perspiciatis saepe similique sunt ullam.</p>
                            </div>
                            <div class="next">
                                <button type="button"  id="next-btn">Next <i class="fa fa-send" aria-hidden="true"></i></button>
                            </div>
                        </div>
                        <div class="step-3-content step-content text-center">
                            <div class="row text-left">
                                <div class="alert alert-danger">
                                    Error, The Total Must Be Equal Or Less Than 100%
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="inputs-row">
                                        <label class="form-check-label">
                                            Lorem ipsum dolor sit amet, consectetur.
                                            <input type="checkbox" class="form-check-input" value="Lorem ipsum dolor sit amet, consectetur." name="percent-checkbox">
                                        </label>
                                        <span class="check"><i class="fa fa-check unchecked" aria-hidden="true"></i></span>
                                        <select name="percent-select" disabled class="form-control">
                                            <option value="0">0%</option>
                                            <option value="10">10%</option>
                                            <option value="20">20%</option>
                                            <option value="30">30%</option>
                                            <option value="40">40%</option>
                                            <option value="50">50%</option>
                                            <option value="60">60%</option>
                                            <option value="70">70%</option>
                                            <option value="80">80%</option>
                                            <option value="90">90%</option>
                                            <option value="100">100%</option>
                                        </select>
                                    </div>
                                    <div class="inputs-row">
                                        <label class="form-check-label">
                                            Lorem ipsum dolor sit amet, consectetur.
                                            <input type="checkbox" class="form-check-input" value="Lorem ipsum dolor sit amet, consectetur." name="percent-checkbox">
                                        </label>
                                        <span class="check"><i class="fa fa-check unchecked" aria-hidden="true"></i></span>
                                        <select name="percent-select" disabled class="form-control">
                                            <option value="0">0%</option>
                                            <option value="10">10%</option>
                                            <option value="20">20%</option>
                                            <option value="30">30%</option>
                                            <option value="40">40%</option>
                                            <option value="50">50%</option>
                                            <option value="60">60%</option>
                                            <option value="70">70%</option>
                                            <option value="80">80%</option>
                                            <option value="90">90%</option>
                                            <option value="100">100%</option>
                                        </select>
                                    </div>
                                    <div class="inputs-row">
                                        <label class="form-check-label">
                                            Lorem ipsum dolor sit amet, consectetur.
                                            <input type="checkbox" class="form-check-input" value="Lorem ipsum dolor sit amet, consectetur." name="percent-checkbox">
                                        </label>
                                        <span class="check"><i class="fa fa-check unchecked" aria-hidden="true"></i></span>
                                        <select name="percent-select" disabled class="form-control">
                                            <option value="0">0%</option>
                                            <option value="10">10%</option>
                                            <option value="20">20%</option>
                                            <option value="30">30%</option>
                                            <option value="40">40%</option>
                                            <option value="50">50%</option>
                                            <option value="60">60%</option>
                                            <option value="70">70%</option>
                                            <option value="80">80%</option>
                                            <option value="90">90%</option>
                                            <option value="100">100%</option>
                                        </select>
                                    </div>
                                    <div class="inputs-row">
                                        <label class="form-check-label">
                                            Lorem ipsum dolor sit amet, consectetur.
                                            <input type="checkbox" class="form-check-input" value="Lorem ipsum dolor sit amet, consectetur." name="percent-checkbox">
                                        </label>
                                        <span class="check"><i class="fa fa-check unchecked" aria-hidden="true"></i></span>
                                        <select name="percent-select" disabled class="form-control">
                                            <option value="0">0%</option>
                                            <option value="10">10%</option>
                                            <option value="20">20%</option>
                                            <option value="30">30%</option>
                                            <option value="40">40%</option>
                                            <option value="50">50%</option>
                                            <option value="60">60%</option>
                                            <option value="70">70%</option>
                                            <option value="80">80%</option>
                                            <option value="90">90%</option>
                                            <option value="100">100%</option>
                                        </select>
                                    </div>
                                    <div class="inputs-row">
                                        <label class="form-check-label">
                                            Lorem ipsum dolor sit amet, consectetur.
                                            <input type="checkbox" class="form-check-input" value="Lorem ipsum dolor sit amet, consectetur." name="percent-checkbox">
                                        </label>
                                        <span class="check"><i class="fa fa-check unchecked" aria-hidden="true"></i></span>
                                        <select name="percent-select" disabled class="form-control">
                                            <option value="0">0%</option>
                                            <option value="10">10%</option>
                                            <option value="20">20%</option>
                                            <option value="30">30%</option>
                                            <option value="40">40%</option>
                                            <option value="50">50%</option>
                                            <option value="60">60%</option>
                                            <option value="70">70%</option>
                                            <option value="80">80%</option>
                                            <option value="90">90%</option>
                                            <option value="100">100%</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="inputs-row">
                                        <label class="form-check-label">
                                            Lorem ipsum dolor sit amet, consectetur.
                                            <input type="checkbox" class="form-check-input" value="Lorem ipsum dolor sit amet, consectetur." name="percent-checkbox">
                                        </label>
                                        <span class="check"><i class="fa fa-check unchecked" aria-hidden="true"></i></span>
                                        <select name="percent-select" disabled class="form-control">
                                            <option value="0">0%</option>
                                            <option value="10">10%</option>
                                            <option value="20">20%</option>
                                            <option value="30">30%</option>
                                            <option value="40">40%</option>
                                            <option value="50">50%</option>
                                            <option value="60">60%</option>
                                            <option value="70">70%</option>
                                            <option value="80">80%</option>
                                            <option value="90">90%</option>
                                            <option value="100">100%</option>
                                        </select>
                                    </div>
                                    <div class="inputs-row">
                                        <label class="form-check-label">
                                            Lorem ipsum dolor sit amet, consectetur.
                                            <input type="checkbox" class="form-check-input" value="Lorem ipsum dolor sit amet, consectetur." name="percent-checkbox">
                                        </label>
                                        <span class="check"><i class="fa fa-check unchecked" aria-hidden="true"></i></span>
                                        <select name="percent-select" disabled class="form-control">
                                            <option value="0">0%</option>
                                            <option value="10">10%</option>
                                            <option value="20">20%</option>
                                            <option value="30">30%</option>
                                            <option value="40">40%</option>
                                            <option value="50">50%</option>
                                            <option value="60">60%</option>
                                            <option value="70">70%</option>
                                            <option value="80">80%</option>
                                            <option value="90">90%</option>
                                            <option value="100">100%</option>
                                        </select>
                                    </div>
                                    <div class="inputs-row">
                                        <label class="form-check-label">
                                            Lorem ipsum dolor sit amet, consectetur.
                                            <input type="checkbox" class="form-check-input" value="Lorem ipsum dolor sit amet, consectetur." name="percent-checkbox">
                                        </label>
                                        <span class="check"><i class="fa fa-check unchecked" aria-hidden="true"></i></span>
                                        <select name="percent-select" disabled class="form-control">
                                            <option value="0">0%</option>
                                            <option value="10">10%</option>
                                            <option value="20">20%</option>
                                            <option value="30">30%</option>
                                            <option value="40">40%</option>
                                            <option value="50">50%</option>
                                            <option value="60">60%</option>
                                            <option value="70">70%</option>
                                            <option value="80">80%</option>
                                            <option value="90">90%</option>
                                            <option value="100">100%</option>
                                        </select>
                                    </div>
                                    <div class="inputs-row">
                                        <label class="form-check-label">
                                            Lorem ipsum dolor sit amet, consectetur.
                                            <input type="checkbox" class="form-check-input" value="Lorem ipsum dolor sit amet, consectetur." name="percent-checkbox">
                                        </label>
                                        <span class="check"><i class="fa fa-check unchecked" aria-hidden="true"></i></span>
                                        <select name="percent-select" disabled class="form-control">
                                            <option value="0">0%</option>
                                            <option value="10">10%</option>
                                            <option value="20">20%</option>
                                            <option value="30">30%</option>
                                            <option value="40">40%</option>
                                            <option value="50">50%</option>
                                            <option value="60">60%</option>
                                            <option value="70">70%</option>
                                            <option value="80">80%</option>
                                            <option value="90">90%</option>
                                            <option value="100">100%</option>
                                        </select>
                                    </div>
                                    <div class="inputs-row">
                                        <label class="form-check-label">
                                            Lorem ipsum dolor sit amet, consectetur.
                                            <input type="checkbox" class="form-check-input" value="Lorem ipsum dolor sit amet, consectetur." name="percent-checkbox">
                                        </label>
                                        <span class="check"><i class="fa fa-check unchecked" aria-hidden="true"></i></span>
                                        <select name="percent-select" disabled class="form-control">
                                            <option value="0">0%</option>
                                            <option value="10">10%</option>
                                            <option value="20">20%</option>
                                            <option value="30">30%</option>
                                            <option value="40">40%</option>
                                            <option value="50">50%</option>
                                            <option value="60">60%</option>
                                            <option value="70">70%</option>
                                            <option value="80">80%</option>
                                            <option value="90">90%</option>
                                            <option value="100">100%</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="next">
                                <button type="button"  id="next-btn">Next <i class="fa fa-send" aria-hidden="true"></i></button>
                            </div>
                        </div>
                        <div class="step-4-content step-content text-center">
                            <div class="row text-center">
                                <div class="col-md-12">
                                    <div class="input">
                                        <input type="text" data-required="true" name="u-name" placeholder="Your Name" class="form-control">
                                    </div>
                                    <div class="input">
                                        <input type="email" data-required="true" name="u-email" placeholder="Your Email" class="form-control">
                                    </div>

                                </div>
                            </div>
                            <div class="next">
                                <button type="button"  id="next-btn">Next <i class="fa fa-send" aria-hidden="true"></i></button>
                            </div>
                        </div>
                        <div class="step-5-content step-content text-center">
                            <div class="title">Lorem ipsum dolor sit amet, consectetur.</div>
                            <div class="input">
                                <div class="row text-center">
                                    <div class="col-md-9 text-left">
                                        <label for="q-1">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing ?
                                        </label>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="input-with-icon">
                                            <input type="number" class="form-control" id="q-1" name="q-1" data-required="true">
                                            <i class="fa fa-dollar" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="input">
                                <div class="row text-center">
                                    <div class="col-md-9 text-left">
                                        <label for="q-1">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing ?
                                        </label>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="input-with-icon">
                                            <input type="number" class="form-control" id="q-1" name="q-1" data-required="true">
                                            <i class="fa fa-dollar" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="input">
                                <div class="row text-center">
                                    <div class="col-md-9 text-left">
                                        <label for="q-1">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing ?
                                        </label>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="input-with-icon">
                                            <input type="number" class="form-control" id="q-1" name="q-1" data-required="true">
                                            <i class="fa fa-dollar" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="input">
                                <div class="row text-center">
                                    <div class="col-md-9 text-left">
                                        <label for="q-1">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing ?
                                        </label>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="input-with-icon">
                                            <input type="number" class="form-control" id="q-1" name="q-1" data-required="true">
                                            <i class="fa fa-dollar" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="input">
                                <div class="row text-center">
                                    <div class="col-md-9 text-left">
                                        <label for="q-1">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing ?
                                        </label>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="input-with-icon">
                                            <input type="number" class="form-control" id="q-1" name="q-1" data-required="true">
                                            <i class="fa fa-dollar" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="title">Or Upload PDF File</div>
                            <input type="file" class="form-control pdf-file" name="pdf-file" accept="application/pdf">
                            <div class="next">
                                <button type="button"  id="next-btn">Next <i class="fa fa-send" aria-hidden="true"></i></button>
                            </div>
                        </div>
                        <div class="step-6-content step-content text-center">
                            <div class="row text-center">
                                <div class="col-md-12">
                                    <div class="input">
                                        <input type="text" data-required="true" name="u-full-name" placeholder="Your Full Name" class="form-control">
                                    </div>
                                    <div class="input">
                                        <input type="text" data-required="true" name="u-company" placeholder="Your Company Name" class="form-control">
                                    </div>
                                    <div class="input">
                                        <input type="text" data-required="false" name="u-phone" placeholder="Your Phone (optional)" class="form-control">
                                    </div>

                                </div>
                            </div>
                            <div class="progress-wrap">
                                <p>Please wait, data is being sent...</p>
                                <div class="progress" style="width: 80%;margin: auto">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%"></div>
                                </div>
                            </div>

                            <div class="next">
                                <button type="button"  id="next-btn">Send <i class="fa fa-send" aria-hidden="true"></i></button>
                            </div>
                        </div>
                        <div class="step-7-content step-content text-center">
                            <div class="alert alert-success">
                                Thanks, We Will Replay Shortly .
                            </div>
                        </div>

                    </div>

                </div>

            </div>
        </form>

    </div>
</div>

<?php get_footer(); ob_end_flush() ?>
