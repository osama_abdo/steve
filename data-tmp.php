<?php
ob_start();
//template name:Show Data
get_header();
if ( !current_user_can( 'manage_options' ) ) {
//	wp_redirect( home_url( '/' ) );
}
?>

<div id="table">
	<h3 class="title">Show Data</h3>
    <div class="table-container">
        <table
                data-toolbar="#toolbar"
                data-toggle="table"
                data-pagination="true"
                data-search="true"
                data-onlyInfoPagination="true"
                data-locale="en-US"
                data-show-refresh="true"
                data-show-export="true"
                data-show-columns="true"
                data-minimum-count-columns="2"
                data-id-field="id"
                data-page-list="[3, 5 ,10, ALL]"
                data-show-footer="false"
        >
            <thead>
            <tr>
                <th scope="col" data-field="id">#</th>
                <th scope="col" data-field="full-name">Full Name</th>
                <th scope="col" data-field="company-name">Company Name</th>
                <th scope="col" data-field="email">Email</th>
                <th scope="col" data-field="phone">Phone</th>
                <th scope="col" data-field="checkbox-checked">checkbox checked</th>
                <th scope="col" data-field="select-box-1">Select Box 1</th>
                <th scope="col" data-field="select-box-1">Select Box 2</th>
                <th scope="col" data-field="question1">Question1</th>
                <th scope="col" data-field="question2">Question2</th>
                <th scope="col" data-field="question3">Question3</th>
                <th scope="col" data-field="question4">Question4</th>
                <th scope="col" data-field="question5">Question5</th>
                <th scope="col" data-field="pdf-file">PDF File</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $data =  new WP_Query(array(
	            'post_type' => 'data'
            ));
            while ($data->have_posts()) : $data->the_post(); ?>
                <tr>
                    <td scope="row"><?php echo $data->current_post + 1 ?></td>
                    <td><?php the_field('full_name'); ?><?php// print_r(get_post_meta(get_the_ID())) ?></td>
                    <td><?php the_field('company_name'); ?></td>
                    <td><?php the_field('email'); ?></td>
                    <td><?php the_field('phone'); ?></td>
                    <td>
                        <p>- <?php the_field('check_box_1'); ?> And Value is: <?php the_field('check_box_1_select'); ?> %</p>
                        <p>- <?php the_field('check_box_2'); ?> And Value is: <?php the_field('check_box_2_select'); ?> %</p>
                        <p>- <?php the_field('check_box_3'); ?> And Value is: <?php the_field('check_box_3_select'); ?> %</p>
                    </td>
                    <td><?php the_field('select_box_1'); ?></td>
                    <td><?php the_field('select_box_2'); ?></td>
                    <td><?php the_field('question_1'); ?></td>
                    <td><?php the_field('question_2'); ?></td>
                    <td><?php the_field('question_3'); ?></td>
                    <td><?php the_field('question_4'); ?></td>
                    <td><?php the_field('question_5'); ?></td>
                    <td><a href="<?php echo(get_field('pdf_file'));?>"><i class="fa fa-file"></i></a></td>
                </tr>
            <?php endwhile; wp_reset_query() ?>
            </tbody>
        </table>
    </div>

</div>


<?php get_footer();ob_end_flush(); ?>
