<?php wp_footer() ?>

<?php if (@$_GET['msg'] == 'success') : ?>
    <script>
        (function ($) {
            $('#steps .steps-tabs .step-tab.active').removeClass('active');
            $('#steps .steps-tabs .step-tab.last-step').addClass('active');
            $('#steps .steps-content .step-content.active').hide().removeClass('active');
            $('#steps .steps-content .step-content:last-of-type').addClass('active').fadeIn(0);
            //$('#steps .steps-content .step-content.active').removeClass('active').next().addClass('active');
        })(jQuery);

    </script>
<?php endif; ?>



<script>
    (function ($) {
        var $table = $('#table'),
            $remove = $('#remove'),
            selections = [];

        function getIdSelections() {
            return $.map($table.bootstrapTable('getSelections'), function (row) {
                return row.id
            });
        }
        function responseHandler(res) {
            $.each(res.rows, function (i, row) {
                row.state = $.inArray(row.id, selections) !== -1;
            });
            return res;
        }
        function detailFormatter(index, row) {
            var html = [];
            $.each(row, function (key, value) {
                html.push('<p><b>' + key + ':</b> ' + value + '</p>');
            });
            return html.join('');
        }
        function operateFormatter(value, row, index) {
            return [
                '<a class="like" href="javascript:void(0)" title="Like">',
                '<i class="glyphicon glyphicon-heart"></i>',
                '</a>  ',
                '<a class="remove" href="javascript:void(0)" title="Remove">',
                '<i class="glyphicon glyphicon-remove"></i>',
                '</a>'
            ].join('');
        }
        window.operateEvents = {
            'click .like': function (e, value, row, index) {
                alert('You click like action, row: ' + JSON.stringify(row));
            },
            'click .remove': function (e, value, row, index) {
                $table.bootstrapTable('remove', {
                    field: 'id',
                    values: [row.id]
                });
            }
        };
        function totalTextFormatter(data) {
            return 'Total';
        }
        function totalNameFormatter(data) {
            return data.length;
        }
        function totalPriceFormatter(data) {
            var total = 0;
            $.each(data, function (i, row) {
                total += +(row.price.substring(1));
            });
            return '$' + total;
        }
        function getHeight() {
            return $(window).height() - $('h1').outerHeight(true);
        }

    })(jQuery)
</script>


</body>
</html>